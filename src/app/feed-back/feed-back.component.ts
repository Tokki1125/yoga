import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-feed-back',
  templateUrl: './feed-back.component.html',
  styleUrls: ['./feed-back.component.scss']
})
export class FeedBackComponent implements OnInit {

  starValue = 0;
  comment = new FormControl();
  constructor(public dialogRef: MatDialogRef<FeedBackComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { name: string, title: string }) { }
  ngOnInit() {
  }

  onStart(value) {
    this.starValue = value;
  }

  onClose() {
    const review = this.comment.value;
    const rate = this.starValue;
    this.dialogRef.close({
      review,
      rate
    })
  }
}
