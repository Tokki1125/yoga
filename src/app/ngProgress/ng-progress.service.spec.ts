import { TestBed, inject } from '@angular/core/testing';

import { NgProgressService } from './ng-progress.service';

describe('NgProgressService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgProgressService]
    });
  });

  it('should be created', inject([NgProgressService], (service: NgProgressService) => {
    expect(service).toBeTruthy();
  }));
});
