import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NgProgressService {
  isShow = false;
  isConfirmDialogShow = false;
  isYesClick$ = new Subject();
  constructor() { }

  start() {
    this.isShow = true;
  }

  done() {
    this.isShow = false;
  }

  clickYes() {
    this.isYesClick$.next(true);
  }

  clickNo() {
    this.isYesClick$.next(false)
  }

  showConfirmDialog() {
    this.isConfirmDialogShow = true;
  }

  hideConfirmDialog() {
    this.isConfirmDialogShow = false;
  }
}
