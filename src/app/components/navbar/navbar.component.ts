import { Component, OnInit, ElementRef } from '@angular/core';
import { ROUTES } from '../sidebar/sidebar.component';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth'
import { Store } from '@ngxs/store';
import { Navigate } from '@ngxs/router-plugin';
import { AuthAction } from 'app/store/auth/auth.actions';
import { AuthState, User } from 'app/store/auth/auth.state';
import { PostState } from 'app/store/post/post.state';
import {TranslateService} from '@ngx-translate/core';
// import {_} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';changeLanguage()
// import defaultLanguage from "../../../assets/i18n/en.json";
@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.css']
}) 
export class NavbarComponent implements OnInit {
    public currencies = ['$ USD', '€ EUR'];
    public currency:any;
    public flags = [
      { name:'English', image: 'assets/img/flags/gb.svg' },
      { name:'German', image: 'assets/img/flags/de.svg' },
    //   { name:'French', image: 'assets/img/flags/fr.svg' },
    //   { name:'Russian', image: 'assets/img/flags/ru.svg' },
    //   { name:'Turkish', image: 'assets/img/flags/tr.svg' }
    ]
    public flag:any;


    private listTitles: any[];
    location: Location;
    mobile_menu_visible: any = 0;
    private toggleButton: any;
    private sidebarVisible: boolean;
    currentUser: User;
    constructor(
        private store: Store,
        location: Location,
        private element: ElementRef,
        private router: Router,
        private fireAuth: AngularFireAuth,
        private translate: TranslateService
    ) {
        this.location = location;
        this.sidebarVisible = false;
        var g_language = window.localStorage.getItem('g_language');
        translate.setDefaultLang(g_language);
        
        
    }
    useLanguage(language: string) {
        this.translate.use(language);
      }
    ngOnInit() {
        this.currency = this.currencies[0];
        this.flag = this.flags[0];  


        this.listTitles = ROUTES.filter(listTitle => listTitle);
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
        this.router.events.subscribe((event) => {
            this.sidebarClose();
            var $layer: any = document.getElementsByClassName('close-layer')[0];
            if ($layer) {
                $layer.remove();
                this.mobile_menu_visible = 0;
            }
        });
        this.store.select(AuthState.currentUser).subscribe((resp) => {
            this.currentUser = resp;
        })
        var l = window.localStorage.getItem('g_language');
        if (l==='en'){
            // window.localStorage.setItem('g_language', 'de');
            this.language = { name:'English', image: 'assets/img/flags/gb.svg' };
        } else {
            // window.localStorage.setItem('g_language', 'en');
            this.language = { name:'German', image: 'assets/img/flags/de.svg' };
        }

    }

    public changeCurrency(currency){
        this.currency = currency;
      }
    
      public changeLang(flag){
        this.flag = flag;
        var l = window.localStorage.getItem('g_language');
        if(flag.name === 'English'){
            window.localStorage.setItem('g_language', 'en');location.reload();      
        }
        if(flag.name === 'German'){
            window.localStorage.setItem('g_language', 'de');location.reload();       
        }


        // if (l === 'en') {
        //   window.localStorage.setItem('g_language', 'de');
        //   this.language1 = 'English';
        //   this.language2 = 'German';
        // } else {
        //   window.localStorage.setItem('g_language', 'en');
        //   this.language1 = 'German';
        //   this.language2 = 'English';
        // }
        // location.reload();
      }
      language1:string;
      language2:string;

    




    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const body = document.getElementsByTagName('body')[0];
        setTimeout(function () {
            toggleButton.classList.add('toggled');
        }, 500);

        body.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const body = document.getElementsByTagName('body')[0];
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        body.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        var $toggle = document.getElementsByClassName('navbar-toggler')[0];

        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
        const body = document.getElementsByTagName('body')[0];

        if (this.mobile_menu_visible == 1) {
            // $('html').removeClass('nav-open');
            body.classList.remove('nav-open');
            if ($layer) {
                $layer.remove();
            }
            setTimeout(function () {
                $toggle.classList.remove('toggled');
            }, 400);

            this.mobile_menu_visible = 0;
        } else {
            setTimeout(function () {
                $toggle.classList.add('toggled');
            }, 430);

            var $layer = document.createElement('div');
            $layer.setAttribute('class', 'close-layer');


            if (body.querySelectorAll('.main-panel')) {
                document.getElementsByClassName('main-panel')[0].appendChild($layer);
            } else if (body.classList.contains('off-canvas-sidebar')) {
                document.getElementsByClassName('wrapper-full-page')[0].appendChild($layer);
            }

            setTimeout(function () {
                $layer.classList.add('visible');
            }, 100);

            $layer.onclick = function () { //asign a function
                body.classList.remove('nav-open');
                this.mobile_menu_visible = 0;
                $layer.classList.remove('visible');
                setTimeout(function () {
                    $layer.remove();
                    $toggle.classList.remove('toggled');
                }, 400);
            }.bind(this);

            body.classList.add('nav-open');
            this.mobile_menu_visible = 1;

        }
    };

    getTitle() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(2);
        }
        titlee = titlee.split('/').pop();

        for (var item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        return 'Qvoot';
    }

    get isLogin() {
        return this.fireAuth.auth.currentUser;
    }

    logout() {
        this.store.dispatch(new AuthAction.SignOut());
    }
    
    // public language: string = window.localStorage.getItem('g_language');
    public language: any;

    
    
    
    
    // public changeLanguage(): void {
    //     var l = window.localStorage.getItem('g_language');
    //     if (l === 'de'){
    //         this.language = 'German';
    //     } else {
    //         this.language = 'English';
    //     };
    //     if(this.language === 'English') { 
    //       this.language = 'German';
    //       window.localStorage.setItem('g_language', 'en');
    //       this.useLanguage('en');
    //     } else {
    //       this.language = 'English';
    //       window.localStorage.setItem('g_language', 'de');
    //       this.useLanguage('de');
    //     }
    //     location.reload();

    // }
    public changeLanguage(): void {
        var l = window.localStorage.getItem('g_language');
        if (l==='en'){
            window.localStorage.setItem('g_language', 'de');
            this.language = { name:'English', image: 'assets/img/flags/gb.svg' };
        } else {
            window.localStorage.setItem('g_language', 'en');
            this.language = { name:'German', image: 'assets/img/flags/de.svg' };
        }
        location.reload();
    }

}
