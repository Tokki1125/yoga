import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { AuthState } from 'app/store/auth/auth.state';
import { Router, RouterModule } from '@angular/router';
import { PostState } from 'app/store/post/post.state';
import { AuthAction } from 'app/store/auth/auth.actions';
import {TranslateService} from '@ngx-translate/core';
// import {_} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';

declare const $: any;
declare interface RouteInfo {
  id: number;
  path: string;
  title: string;
  icon: string;
  class: string;
  isShow?: boolean;
  isAdmin?: boolean;
  isMobile?: boolean;
  login?: boolean;
  fnCall?: () => void;
}
export let MobileRoute: RouteInfo[] = [
  { id: 1, path: '/about-us', title: 'About us', icon: 'library_books', class: '', isShow: true, login: false },
  // { id: 1, path: '/about-us', title: this.translate.instant('toast.wtq'), icon: 'library_books', class: '', isShow: true, login: false },
  { id: 1, path: '/how-works', title: 'How It Works', icon: 'library_books', class: '', isShow: true, login: false },
  { id: 1, path: '/contact-us', title: 'Contact us', icon: 'library_books', class: '', isShow: true, login: false },
  { id: 1, path: '/blog-list', title: 'blogs', icon: 'library_books', class: '', isShow: true, login: false },

] 
export let ROUTES: RouteInfo[] = [
  { id: 1, path: `/user-profile`, title: 'Information', icon: 'person', class: '', isShow: true },
  { id: 2, path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '', isShow: false },
  { id: 3, path: '/post', title: 'Add Post', icon: 'library_books', class: '', isShow: true },
  { id: 4, path: '/my-post', title: 'My Post', icon: 'library_books', class: '', isShow: true },
  { id: 7, path: '/calendar', title: 'calendar', icon: 'library_books', class: '', isShow: true,isAdmin: true},
  { id: 5, path: '/my-purchases', title: 'My bookings', icon: 'library_books', class: '', isShow: true },
  { id: 6, path: '/add-blog', title: 'Add Blog', icon: 'library_books', class: '', isShow: false, isAdmin: true },
  { id: 7, path: '/update-contact', title: 'Contact us', icon: 'library_books', class: '', isShow: false, isAdmin: true },
  { id: 7, path: '/update-aboutus', title: 'About us', icon: 'library_books', class: '', isShow: false, isAdmin: true },
  { id: 7, path: '/partners', title: 'Partners', icon: 'library_books', class: '', isShow: true},



  { id: 4, path: '/table-list', title: 'Table List', icon: 'content_paste', class: '', isShow: false },
  { id: 5, path: '/typography', title: 'Typography', icon: 'library_books', class: '', isShow: false },
  { id: 6, path: '/icons', title: 'Icons', icon: 'bubble_chart', class: '', isShow: false },
  { id: 7, path: '/maps', title: 'Maps', icon: 'location_on', class: '', isShow: false },
  { id: 8, path: '/notifications', title: 'Notifications', icon: 'notifications', class: '', isShow: false },
  { id: 9, path: '/upgrade', title: 'Upgrade to PRO', icon: 'unarchive', class: 'active-pro', isShow: false },
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  public currencies = ['$ USD', '€ EUR'];
  public currency:any;
  public flags = [
    { name:'English', image: 'assets/img/flags/gb.svg' },
    { name:'German', image: 'assets/img/flags/de.svg' },
  //   { name:'French', image: 'assets/img/flags/fr.svg' },
  //   { name:'Russian', image: 'assets/img/flags/ru.svg' },
  //   { name:'Turkish', image: 'assets/img/flags/tr.svg' }
  ]
  public flag:any;
  menuItems: any[];
  public language: any;
  constructor(private translate: TranslateService,private store: Store) {
    var g_language = window.localStorage.getItem('g_language');
    translate.setDefaultLang(g_language);
    
  }

  useLanguage(language: string) {
    this.translate.use(language);
  }
  public changeCurrency(currency){
    this.currency = currency;
  }

  public changeLang(flag){
    this.flag = flag;
    var l = window.localStorage.getItem('g_language');
    if(flag.name === 'English'){
        window.localStorage.setItem('g_language', 'en');location.reload();      
    }
    if(flag.name === 'German'){
        window.localStorage.setItem('g_language', 'de');location.reload();       
    }


    // if (l === 'en') {
    //   window.localStorage.setItem('g_language', 'de');
    //   this.language1 = 'English';
    //   this.language2 = 'German';
    // } else {
    //   window.localStorage.setItem('g_language', 'en');
    //   this.language1 = 'German';
    //   this.language2 = 'English';
    // }
    // location.reload();
  }
  ngOnInit() {
    this.currency = this.currencies[0];
    this.flag = this.flags[0];
    var l = window.localStorage.getItem('g_language');
    if (l==='en'){
        // window.localStorage.setItem('g_language', 'de');
        this.language = { name:'English', image: 'assets/img/flags/gb.svg' };
    } else {
        // window.localStorage.setItem('g_language', 'en');
        this.language = { name:'German', image: 'assets/img/flags/de.svg' };
    }
    if (this.isMobileMenu()) {
      this.menuItems = [...ROUTES.filter(menuItem => menuItem.isShow), ...MobileRoute]
    } else {
      this.menuItems = ROUTES.filter(menuItem => menuItem.isShow);

    }
    this.store.select(AuthState.currentUser).subscribe((resp) => {

      if (resp && resp.isAdmin) {
        if (this.isMobileMenu()) {
          this.menuItems = [...ROUTES.filter(menuItem => menuItem.isShow || menuItem.isAdmin), ...MobileRoute, this.getLogout()];
        } else {
          this.menuItems = ROUTES.filter(menuItem => menuItem.isShow || menuItem.isAdmin)

        }
      } else if (resp) {
        if (this.isMobileMenu()) {
          this.menuItems = [...ROUTES.filter(menuItem => menuItem.isShow), ...MobileRoute, this.getLogout()];
        } else {
          this.menuItems = ROUTES.filter(menuItem => menuItem.isShow)

        }

      } else {

        this.menuItems = [...MobileRoute,this.getLogin()]
      }
    })

  }
  getLogout() {
    return {
      id: 1, path: '',
      fnCall: () => { this.store.dispatch(new AuthAction.SignOut()) },
      title: 'logout', icon: 'library_books', class: '', isShow: true, login: false
    };
  }
  getLogin() {
    return {
      id: 1, path: '/login',
      title: 'Login', icon: 'library_books', class: '', isShow: true, login: false
    };
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
