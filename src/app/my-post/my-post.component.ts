import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';
import { PostState, Post } from 'app/store/post/post.state';

@Component({
  selector: 'app-my-post',
  templateUrl: './my-post.component.html',
  styleUrls: ['./my-post.component.scss']
})
export class MyPostComponent implements OnInit {
  posts: Post[] = [];
  constructor(private store: Store) {
    this.store.dispatch(new PostAction.FetchMyPost());
    this.store.select(PostState.myPost).subscribe((resp) => {
      this.posts = resp;
    })
  }

  ngOnInit() {
  }

}
