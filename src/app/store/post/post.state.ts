import { State, Action, Store, StateContext, Selector } from '@ngxs/store';
import { PostAction } from './post.actions';
import { AngularFirestore } from '@angular/fire/firestore';
import { Fire_Collect } from 'app/firebase.constant';
import { Navigate } from '@ngxs/router-plugin';
import { take, tap, map, finalize } from 'rxjs/operators';
import { User, AuthState } from '../auth/auth.state';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { zip, Observable, combineLatest } from 'rxjs';
import { LoaderService } from 'app/loader.service';
import { ToastrService } from 'ngx-toastr';
import { AuthAction } from '../auth/auth.actions';
import { ConfirmDailogComponent } from 'app/confirm-dailog/confirm-dailog.component';
import { MatDialog } from '@angular/material';
import { NgProgressService } from 'app/ngProgress/ng-progress.service';

export class TransactionRecord {
    addedDateTime = new Date().getTime();
    endDate: number;
    postId: string;
    reviewGiven = false;
    startDate: number;
    totalCost: number;
    totalDays: number;
    transactionId: string;
    userId: string;
}

export enum RateType {
    ratePerHour = 'per hour',
    ratePerNight = 'per day',
}
export enum type {
    yoga = 'Yoga Class',
    bodybuilding = 'Body building',
    souna = 'Souna',
    haircut = 'Hair Cut'

}

export class Post {
    userid: string;
    type: string;
    title: string;
    status: number;
    imageList: string[];
    id: string;
    description: string;
    date: string;
    city: string;
    postalCode?: string;
    street?: string;
    country?: string;
    user?: User;
    rateType: RateType;
    rateValue: number;
    currency: string;
}

export class Purchases extends Post {
    purId: string;
    startDate: number;
    endDate: number;
    addDateTime: number;
    reviewGiven: boolean;
    totalCost: number;
    postId: string;
}
export class Chat {

    fileDownload = false;
    fileStatus = 'uploading';
    from: string;
    id?: string;
    message: string;
    messageStatus = 0;
    messageType = 5;
    seekBarPosition: number;
    timestamp = new Date().getTime();
    uploadingProgress = 0;
    userId = 0;
    isMyMessage: boolean;
    to: string;
    user?: User;
}

export class PostFeedback {
    date: number;
    postId: string;
    postRating: number;
    postReview: string;
    userId: string;
    reviewFromUser: User;
}

export class UserFeedback {
    date: number;
    postId: string;
    rating: number;
    review: string;
    reviewFrom: string;
    reviewToId: string;
    reviewFromUser?: User;
}
export class ContactUs {
    name: string;
    email: string;
    note: string;
    date: number;
}

export class Contact {
    phone: string;
    email: string;
    address: string;
}
export class Blog {
    title: string;
    note: string;
    date: number;
}

export class Aboutus {
    introduction: string;
    mission: string;
    vision:string;
}
export class PostStateModel {
    posts: Post[] = [];
    selectedPost: Post;
    selectedPostFeedback: PostFeedback[] = [];
    selectedUser: User;
    selectedChat: Chat[] = [];
    lastChatList: Chat[] = [];
    myPost: Post[] = [];
    blogList: Blog[] = [];
    contactList: ContactUs[] = [];
    TransactionRecList: TransactionRecord[] = [];
    myPurchases: Purchases[] = [];
    unRatePurchases: Purchases[] = [];
    checkingLastMessage: Chat;
    contact: Contact;
    aboutus: Aboutus;
    postForm: {
        model: any,
        dirty: false,
        status: '',
        errors: {}
    }
    contactForm: {
        model: any,
        dirty: false,
        status: '',
        errors: {}
    }
    blogForm: {
        model: any,
        dirty: false,
        status: '',
        errors: {}
    }
    aboutusForm: {
        model: any,
        dirty: false,
        status: '',
        errors: {}
    }
}


@State<PostStateModel>({
    name: 'post',
    defaults: new PostStateModel(),
})
export class PostState {
    @Selector()
    static posts(state: PostStateModel) { return state.posts }

    @Selector()
    static selectPost(state: PostStateModel) { return state.selectedPost }
    @Selector()
    static selectChat(state: PostStateModel) { return state.selectedChat }
    @Selector()
    static lastChatList(state: PostStateModel) { return state.lastChatList }
    @Selector()
    static myPost(state: PostStateModel): Post[] { return state.myPost }
    @Selector()
    static myPurchaseList(state: PostStateModel): Purchases[] { return state.myPurchases }
    @Selector()
    static unRatePurchaseList(state: PostStateModel): Purchases[] { return state.unRatePurchases }
    @Selector()
    static selectPostFeedbacks(state: PostStateModel): PostFeedback[] { return state.selectedPostFeedback }
    @Selector()
    static blogList(state: PostStateModel): Blog[] { return state.blogList }
    @Selector()
    static contactList(state: PostStateModel): ContactUs[] { return state.contactList }
    @Selector()
    static checkLastMessage(state: PostStateModel): Chat { return state.checkingLastMessage }
    @Selector()
    static contact(state: PostStateModel): Contact { return state.contact }
    @Selector()
    static aboutus(state: PostStateModel): Aboutus { return state.aboutus }
    constructor(
        public afStore: AngularFirestore,
        public store: Store,
        public afRDB: AngularFireDatabase,
        public afAuth: AngularFireAuth,
        public loader: LoaderService,
        public ngProgress: NgProgressService,
        private toastr: ToastrService,
        public dialog: MatDialog) {

    }
    get currentUser() {
        return this.afAuth.auth.currentUser;
    }
    checkIfEmailInString(text) {
        var re = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
        return re.test(text);
    }
    checkforPhoneNumber(text) {
        var phoneExp = /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?/img;
        return phoneExp.test(text);
    }
    checkForUrl(text) {
        var urlRegex = /(https?:\/\/[^\s]+)/g;
        return urlRegex.test(text)
    }
    @Action(PostAction.SendMessage)
    sendMessage(ctx: StateContext<PostStateModel>, payload: PostAction.SendMessage) {
        const currentUser = this.afAuth.auth.currentUser;
        if (this.checkIfEmailInString(payload.payload.message)) {
            this.toastr.warning('Email is not allow to send.');
            return;
        }
        if (this.checkforPhoneNumber(payload.payload.message)) {
            this.toastr.warning('Phone is not allow to send.');
            return;
        }
        if (this.checkForUrl(payload.payload.message)) {
            this.toastr.warning('URL is not allow to send.');
            return;
        }

        if (typeof payload.payload.message === 'string' && payload.payload.message.includes('@')) {
            this.toastr.warning('@ Sign is not allowed');
            return;
        }
        const chatInstance: Chat = {
            ...new Chat(),
            from: currentUser.uid,
            message: payload.payload.message,
            to: payload.payload.to,
        }
        let lastMessageId = payload.payload.lastMessageId
        if (!lastMessageId) {
            lastMessageId = `${currentUser.uid}_${payload.payload.to}`;
        }
        this.afRDB.list(`Chat/${lastMessageId}`).push(
            chatInstance
        )
        this.afRDB.object(`LastMessage/${lastMessageId}`).update(chatInstance);
    }

    @Action(PostAction.CheckForKeyLastMessage)
    checkForKeyLastMessage(ctx: StateContext<PostStateModel>, payload: PostAction.CheckForKeyLastMessage) {
        const currentUserId = this.currentUser.uid;;
        const cuau = payload.antherUserId;
        return this.afRDB.list(`LastMessage`).snapshotChanges().pipe(tap((resp) => {
            const selectItem = resp.find((item) => {
                const id = item.key;
                return id.includes(currentUserId) && id.includes(cuau);
            })
            if (selectItem) {
                const id = selectItem.key;
                const data = selectItem.payload.val() as Chat;
                ctx.patchState({
                    checkingLastMessage: {
                        ...data,
                        id,
                    }
                })
            } else {
                ctx.patchState({
                    checkingLastMessage: null,
                })
            }
        }))
    }
    @Action(PostAction.FetchSelectedChat)
    selectedChat(ctx: StateContext<PostStateModel>, payload: PostAction.FetchSelectedChat) {
        const currentUser = this.afAuth.auth.currentUser;
        const id = payload.payload.lastMessageId;
        const selectedUser = this.afStore.doc(`Users/${payload.payload.id}`).valueChanges() as Observable<User>;
        const selectChat: Observable<Chat[]> = this.afRDB.list(`Chat/${id}`).valueChanges() as Observable<Chat[]>;
        const loginUser = this.afStore.doc(`Users/${currentUser.uid}`).valueChanges() as Observable<User>;

        return combineLatest(selectChat, selectedUser, loginUser)
            .pipe(
                tap(([chat, user, cUser]) => {
                    const chatArray = chat.map((item) => ({ ...item, isMyMessage: item.from === currentUser.uid }))
                    const finalChat = chatArray.map((item) => {
                        if (item.isMyMessage) {
                            return {
                                ...item,
                                user: cUser,
                                id,
                            }
                        } else {
                            return {
                                ...item,
                                user,
                                id,
                            }
                        }
                    })
                    ctx.patchState({
                        selectedChat: finalChat as Chat[],
                    })
                })
            )
    }

    @Action(PostAction.FetchAllChat)
    FetchAllChat(ctx: StateContext<PostStateModel>) {
        this.ngProgress.start();
        const lastMessage$ = this.afRDB.list('LastMessage').snapshotChanges();
        const allUser$ = this.afStore.collection('Users').snapshotChanges();
        return combineLatest(lastMessage$, allUser$)
            .pipe(
                map(([lastChat, users]) => {
                    this.ngProgress.done();
                    if (Array.isArray(lastChat)) {
                        const currentUser = this.afAuth.auth.currentUser;

                        const lastMsgs = lastChat.filter((item) => {
                            return item.key.includes(currentUser.uid);
                        }).map((resp) => {
                            const lastmsg = resp.payload.val();
                            return {
                                ...lastmsg,
                                id: resp.key,
                            };

                        }) as Chat[];
                        // user handling
                        const userList: User[] = users.map((item) => {
                            const user = item.payload.doc.data();
                            return {
                                ...user,
                                id: item.payload.doc.id,
                            } as User;
                        }).filter((item) => item.id !== currentUser.uid);

                        const finalLastChatList = lastMsgs.map((cItem) => {
                            const user = userList.find((item) => cItem.id.includes(item.id));
                            return {
                                ...cItem,
                                user,
                            }
                        });
                        ctx.patchState({
                            lastChatList: finalLastChatList,
                        })
                    }
                })
            )
    }
    @Action(PostAction.FetchUserById)
    fetchUserById(ctx: StateContext<PostStateModel>, payload: PostAction.FetchUserById) {
        this.ngProgress.start();;
        const state = ctx.getState();
        if (payload.id) {
            this.afStore
                .doc<User>(`${Fire_Collect.Users}/${payload.id}`)
                .snapshotChanges()
                .pipe(take(1))
                .subscribe((resp) => {
                    this.ngProgress.done();;
                    const user = {
                        ...resp.payload.data(),
                        UID: resp.payload.id,
                    }
                    ctx.patchState({
                        selectedPost: {
                            ...state.selectedPost,
                            user,
                        },
                        selectedUser: user,
                    })
                })
        }
    }
    @Action(PostAction.SelectPost)
    selectPost(ctx: StateContext<PostStateModel>, { payload }: PostAction.SelectPost) {
        this.ngProgress.start();
        return this.afStore.doc<Post>(`${Fire_Collect.Posts}/${payload}`).snapshotChanges()
            .pipe(take(1),
                tap((resp) => {
                    this.ngProgress.done();
                    const data = resp.payload.data();
                    const id = resp.payload.id;
                    ctx.patchState({
                        selectedPost: {
                            ...data,
                            id,
                        },
                    })
                    this.store.dispatch(new PostAction.FetchUserById(data.userid))
                }))
    }

    @Action(PostAction.FetchAll)
    fetchAll(ctx: StateContext<PostStateModel>) {
        this.ngProgress.start();
        return this.afStore
            .collection(Fire_Collect.Posts)
            .snapshotChanges()
            .pipe(
                tap((resp = []) => {
                    this.ngProgress.done();
                    const data = resp.map((action) => {
                        const data = action.payload.doc.data()
                        const id = action.payload.doc.id;
                        return {
                            ...data, id
                        }
                    })
                    ctx.patchState({
                        posts: data as Post[],
                    })
                }))
    }

    @Action(PostAction.SavePost)
    savePost(ctx: StateContext<PostStateModel>) {
        this.ngProgress.start();
        const state = ctx.getState();
        let model = state.postForm.model as Post;
        model = {
            ...model,
            currency: 'Eur',
            type: model.type.toLowerCase(),
        }
        if (model.id) {
            return this.afStore.doc<Post>(`${Fire_Collect.Posts}/${model.id}`)
                .update(model)
                .then((resp) => {
                    this.ngProgress.done();
                    ctx.patchState({
                        posts: [...state.posts]
                    })
                    this.toastr.success('Post save successfully')
                    this.store.dispatch(new Navigate(['/detail', model.id]))
                })
        }
        return this.afStore.collection(Fire_Collect.Posts).add({ ...model, userid: this.currentUser.uid }).then((resp) => {
            this.ngProgress.done();
            ctx.patchState({
                posts: [...state.posts],
            })
            this.ngProgress.showConfirmDialog();
            this.ngProgress.isYesClick$.asObservable().pipe(take(1)).subscribe(() => {
                this.toastr.success('Post save successfully')
                this.store.dispatch(new Navigate(['/home']))
            })
        })
    }

    @Action(PostAction.FetchMyPost)
    FetchMyPost(ctx: StateContext<PostStateModel>) {
        this.ngProgress.start();
        return this.afStore.collection(Fire_Collect.Posts, (ref) => {
            return ref.where('userid', '==', this.currentUser.uid);
        }).snapshotChanges()
            .pipe(
                take(1),
                tap((resp = []) => {
                    this.ngProgress.done();
                    const data = resp.map((action) => {
                        const data = action.payload.doc.data()
                        const id = action.payload.doc.id;
                        return {
                            ...data, id
                        }
                    })
                    ctx.patchState({
                        myPost: data as Post[],
                    })
                }))
    }
    mapPost(snapShot): Post[] {
        return snapShot.map((action) => {
            const data = action.payload.data()
            const id = action.payload.id;
            return {
                ...data, id
            }
        })
    }
    @Action(PostAction.DeletePost)
    deletePost(ctx: StateContext<PostStateModel>, payload: PostAction.DeletePost) {
        this.ngProgress.start();
        return this.afStore.doc<Post>(`Posts/${payload.id}`).delete().then((resp) => {
            this.ngProgress.done();
            this.toastr.success('Delete successfully')
            this.store.dispatch(new Navigate(['my-post']));
        })
    }
    // transition

    @Action(PostAction.Purchesing)
    purchesing(ctx: StateContext<PostStateModel>, payload: PostAction.Purchesing) {
        this.ngProgress.start();
        const record = {
            ...payload.record,
            userId: this.currentUser.uid,
        }
        return this.afStore
            .collection(Fire_Collect.TransactionRecord).add(record)
            .then((resp) => {
                this.toastr.success('Reservation done successfully.')
                this.ngProgress.done();
            })
    }

    @Action(PostAction.FetchMyPurchesing)
    fetchMyPurchasing(ctx: StateContext<PostStateModel>) {
        this.ngProgress.start();
        return this.afStore.collection(Fire_Collect.TransactionRecord, (ref) => {
            return ref.where('userId', '==', this.currentUser.uid);
        }).valueChanges().pipe(take(1), tap((recList: Purchases[]) => {
            const postIdList = recList.map((item) => {
                return this.afStore.doc<Post>(`${Fire_Collect.Posts}/${item.postId}`).snapshotChanges()
            })
            if (!postIdList.length) {
                this.ngProgress.done();;
            }
            zip(...postIdList).subscribe((postResp) => {
                this.ngProgress.done();
                const postL: Post[] = this.mapPost(postResp);
                const purchaseList = recList.map((prItem) => {
                    const p = postL.find((postitem) => postitem.id === prItem.postId);
                    return {
                        ...p,
                        ...prItem,
                    }
                })
                ctx.patchState({
                    myPurchases: purchaseList,
                })
            })
        }))
    }
    @Action(PostAction.FetchUnRatePurches)
    fetchUnRatePurches(ctx: StateContext<PostStateModel>) {
        return this.afStore.collection(Fire_Collect.TransactionRecord, (ref) => {
            return ref
                .where('userId', '==', this.currentUser.uid)
                .where('reviewGiven', '==', false)
                .where('endDate', '<', new Date().getTime());
        }).snapshotChanges().pipe(take(1), tap((resp) => {
            const recList = resp.map((item) => {
                const data = item.payload.doc.data() as Purchases;
                const id = item.payload.doc.id;
                return {
                    ...data,
                    purId: id,
                };
            });
            const postIdList = recList.map((item) => {
                return this.afStore.doc<Post>(`${Fire_Collect.Posts}/${item.postId}`).snapshotChanges()
            })
            zip(...postIdList).subscribe((postResp) => {
                const postL: Post[] = this.mapPost(postResp);

                const purchaseList = recList.map((prItem) => {
                    const p = postL.find((postitem) => postitem.id === prItem.postId);
                    return {
                        ...p,
                        ...prItem,
                    }
                })
                ctx.patchState({
                    unRatePurchases: purchaseList,
                })
            })
        }))
    }

    @Action(PostAction.RatePost)
    ratePost(ctx: StateContext<PostStateModel>, payload: PostAction.RatePost) {
        const rate: Partial<PostFeedback> = {
            ...payload.rate,
            date: new Date().getTime(),
            userId: this.currentUser.uid,
        }
        return this.afStore.collection(Fire_Collect.PostFeedback).add(rate).then((resp) => {
        })
    }

    @Action(PostAction.RateUser)
    rateUser(ctx: StateContext<PostStateModel>, payload: PostAction.RateUser) {
        const rate: Partial<UserFeedback> = {
            ...payload.rate,
            date: new Date().getTime(),
            reviewFrom: this.currentUser.uid,
        }
        return this.afStore.collection(Fire_Collect.UserFeedback).add(rate).then((resp) => {
        })
    }


    @Action(PostAction.RateComplete)
    rateComplete(ctx: StateContext<PostStateModel>, payload: PostAction.RateComplete) {
        return this.afStore
            .doc(`${Fire_Collect.TransactionRecord}/${payload.transationId}`)
            .update({
                reviewGiven: true,
            }).then((resp) => {
            })
    }

    @Action(PostAction.FetchFeedback)
    fetchFeedback(ctx: StateContext<PostStateModel>, payload: PostAction.FetchFeedback) {
        this.ngProgress.start();
        ctx.patchState({
            selectedPostFeedback: [],
        })
        return this.afStore
            .collection(Fire_Collect.PostFeedback, (ref) => ref.where('postId', '==', payload.postId))
            .valueChanges().pipe(tap((resp: PostFeedback[]) => {
                this.ngProgress.done();
                const users$ = resp.map((item) => {
                    return this.afStore.doc(`${Fire_Collect.Users}/${item.userId}`).snapshotChanges()
                })
                zip(...users$).subscribe((res) => {
                    this.ngProgress.done();
                    const users = res.map((item) => {
                        const id = item.payload.id;
                        const user = item.payload.data();
                        return {
                            id,
                            ...user,
                        }
                    })
                    const feedbackList = resp.map((item) => {
                        const reviewFromUser = users.find((i) => i.id === item.userId);
                        return {
                            ...item,
                            reviewFromUser,
                        }
                    })

                    ctx.patchState({
                        selectedPostFeedback: feedbackList as PostFeedback[],
                    })
                })
            }))
    }

    @Action(PostAction.DoContact)
    doContact(ctx: StateContext<PostStateModel>) {
        const state = ctx.getState();
        const model = state.contactForm.model as Post;
        return this.afStore.collection(Fire_Collect.Contactus).add(model).then((resp) => {
        })
    }
    @Action(PostAction.AddBlog)
    addBlog(ctx: StateContext<PostStateModel>) {
        const state = ctx.getState();
        const model = state.blogForm.model as Post;
        return this.afStore.collection(Fire_Collect.Blog).add(model).then((resp) => {
            this.toastr.success('Blog save successfully')

            this.store.dispatch(new Navigate(['blog-list']))
        })
    }

    @Action(PostAction.FetchBlog)
    fetchBlog(ctx: StateContext<PostStateModel>) {
        this.ngProgress.start();
        return this.afStore.collection(Fire_Collect.Blog).valueChanges().pipe(tap((resp) => {
            this.ngProgress.done();
            ctx.patchState({
                blogList: resp as Blog[],
            })
        }))
    }

    @Action(PostAction.updateContact)
    updateContact(ctx: StateContext<PostStateModel>) {
        this.ngProgress.start()
        const { model } = ctx.getState().contactForm;
        return this.afStore.doc(`${Fire_Collect.Contactus}/abc123`).set({
            ...model
        }).then((resp) => {
            this.store.dispatch(new Navigate(['contact-us']))
            this.toastr.success('Contact updated successfully')
            this.ngProgress.done();


        })
    }
    @Action(PostAction.fetchContact)
    updatecontact(ctx: StateContext<PostStateModel>) {
        return this.afStore.doc(`${Fire_Collect.Contactus}/abc123`).valueChanges().pipe(tap(resp => {
            ctx.patchState({
                contact: resp as Contact,
            })
        }))
    }
    @Action(PostAction.FetchContact)
    fetchContact(ctx: StateContext<PostStateModel>) {
        return this.afStore.collection(Fire_Collect.Contactus).valueChanges().pipe(tap((resp) => {
            ctx.patchState({
                contactList: resp as ContactUs[],
            })
        }))
    }

    @Action(PostAction.UpdateAboutus)
    updateAboutus(ctx: StateContext<PostStateModel>) {
        const { model } = ctx.getState().aboutusForm;
        return this.afStore.doc('About/123abc').set(
            model
        ).then(() => {
            this.store.dispatch(new Navigate(['about-us']))
        })
    }

    @Action(PostAction.FetchAboutus)
    fetchaboutus(ctx: StateContext<PostStateModel>) {
        return this.afStore.doc('About/123abc').valueChanges().pipe(tap(resp => {
            ctx.patchState({
                aboutus: resp as Aboutus,
            })
        }))
    }

    @Action(AuthAction.SignOut)
    singOut(ctx: StateContext<PostStateModel>) {
        ctx.setState(new PostStateModel());
    }
}
