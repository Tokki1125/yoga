import { Chat, TransactionRecord, PostFeedback, UserFeedback } from './post.state';

export namespace PostAction {
    export class SavePost {
        static readonly type = '[Post] Save Post';
    }

    export class DeletePost {
        static readonly type = '[Post] Delete Post';
        constructor(public id: string) {

        }
    }
    export class FetchAll {
        static readonly type = '[Post] Fetch all Post'
    }

    export class SelectPost {
        static readonly type = '[Post] Select Post';
        constructor(public payload: any) {

        }
    }
    export class FetchUserById {
        static readonly type = '[Post] Fetch user by id';
        constructor(public id: any) { }
    }

    export class FetchSelectedChat {
        static readonly type = '[Post] Fetch Chat By User';
        constructor(public payload: { id: string, lastMessageId: string }) { }
    }

    export class SendMessage {
        static readonly type = '[Post] Send Message';
        constructor(public payload: { message: string, to: string, lastMessageId: string }) { }
    }

    export class FetchAllChat {
        static readonly type = '[Post] Fetch all Chat';
    }

    export class FetchAllUser {
        static readonly type = '[Post] Fetch All User';
        constructor(public lastChat: Chat[]) {

        }
    }

    export class FetchMyPost {
        static readonly type = '[Post] Fetch My Post';
    }

    export class Purchesing {
        static readonly type = '[Post] Purchesing ';
        constructor(public record: Partial<TransactionRecord>) {

        }
    }
    export class FetchMyPurchesing {
        static readonly type = '[Post] Fetch My Purchesing ';
    }

    export class FetchUnRatePurches {
        static readonly type = '[Post] Fetch un rate purchess';
    }

    export class RatePost {
        static readonly type = '[Post] Post Rate';
        constructor(public rate: Partial<PostFeedback>) { }
    }
    export class RateUser {
        static readonly type = '[Post] User Rate';
        constructor(public rate: Partial<UserFeedback>) { }
    }

    export class RateComplete {
        static readonly type = '[Post] Feedback complete';
        constructor(public transationId: string) { }
    }

    export class FetchFeedback {
        static readonly type = '[Post] fetch feedback';
        constructor(public postId: string) { }

    }

    export class DoContact {
        static readonly type = '[Post] Do contact';
    }
    export class AddBlog {
        static readonly type = '[Post] Add blog';
    }

    export class FetchBlog {
        static readonly type = '[Post] Fetch blog';
    }

    export class FetchContact {
        static readonly type = '[Post] Fetch Contact';
    }

    export class CheckForKeyLastMessage {
        static readonly type = '[Post] check for key last message';
        constructor(public antherUserId: string) { }
    }

    export class updateContact {
        static readonly type = '[Post] update contact';
    }

    export class fetchContact {
        static readonly type = '[Post] Fetch Contact';
    }

    export class FetchAboutus {
        static readonly type = '[Post] fetch about us'
    }

    export class UpdateAboutus {
        static readonly type = '[Post] Update about us';
    }
}
