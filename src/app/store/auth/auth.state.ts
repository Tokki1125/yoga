import { State, Action, StateContext, Store, Selector } from '@ngxs/store';
import { AuthAction } from './auth.actions';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Fire_Collect } from 'app/firebase.constant';
import { Navigate } from '@ngxs/router-plugin';

import {TranslateService} from '@ngx-translate/core';
// import {_} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';



// rxjs
import { take, first, tap, finalize } from 'rxjs/operators';
import { UserFeedback, PostStateModel } from '../post/post.state';
import { zip } from 'rxjs/internal/observable/zip';
import { PostAction } from '../post/post.actions';
import { LoaderService } from 'app/loader.service';
import { ToastrService } from 'ngx-toastr';
import { NgProgressService } from 'app/ngProgress/ng-progress.service';

export class User {
  id?: string;
  email: string;
  image: string;
  name: string;
  phone: string;
  address: string;
  password: string;
  UID: string;
  photoIdType: string;
  photoId: string;
  status: string;
  isAdmin: boolean;
}

export class AuthStateModel {
  currentUser: User;
  selectUser: User;
  selectFeedback: UserFeedback[];
  loginForm: {
    model: any,
    dirty: false,
    status: "",
    errors: { },
  }
  signupForm: {
    model: User,
    dirty: false,
    status: "",
    errors: {},
  }
}

@State<AuthStateModel>({
  name: 'auth',
  defaults: new AuthStateModel(),
})
export class AuthState {
  userCol: AngularFirestoreCollection<User>;
  userDoc: AngularFirestoreDocument<User>
  // selectors
  @Selector()
  static currentUser(state: AuthStateModel): User { return state.currentUser }
  @Selector()
  static selectUser(state: AuthStateModel) { return state.selectUser }
  @Selector()
  static selectFeedBack(state: AuthStateModel): UserFeedback[] { return state.selectFeedback }
  @Selector()
  static loginError(state: AuthStateModel): any { return state.loginForm.errors }

  constructor(
    private translate: TranslateService,
    public afAuth: AngularFireAuth,
    public afStore: AngularFirestore,
    public store: Store,
    public loader: LoaderService,
    public ngProgress: NgProgressService,
    private toastr: ToastrService,
  ) {
    this.userCol = this.afStore.collection<User>(Fire_Collect.Users);
    var g_language = window.localStorage.getItem('g_language');
    translate.setDefaultLang(g_language);
  }
  getLoginUser() {
    return this.afAuth.auth.currentUser;
  }
  // reducers
  useLanguage(language: string) {
    this.translate.use(language);
  }
  @Action(AuthAction.FetchUserById)
  async getUser(ctx: StateContext<AuthStateModel>, { payload }: AuthAction.FetchUserById) {
    let userId = payload;

    if (userId) {
      this.afStore
        .doc<User>(`${Fire_Collect.Users}/${userId}`)
        .valueChanges()
        .pipe(take(1))
        .subscribe((resp) => {
          ctx.patchState({
            selectUser: resp,
          })
        })
    }
  }

  @Action(AuthAction.FetchCurrentUser)
  async getCurrentUser(ctx: StateContext<AuthStateModel>) {
    const user = await this.afAuth.authState.pipe(first()).toPromise();
    const userId = user && user.uid;

    if (userId) {
      this.afStore
        .doc<User>(`${Fire_Collect.Users}/${userId}`)
        .snapshotChanges()
        .pipe(take(1))
        .subscribe((resp) => {
          const UID = resp.payload.id;
          const userData = resp.payload.data();
          ctx.patchState({
            currentUser: {
              UID,
              ...userData,
            },
          })
        })
    }
  }
  @Action(AuthAction.Signup)
  signup(ctx: StateContext<AuthStateModel>) {
    this.ngProgress.start();
    const state = ctx.getState();
    const model = state.signupForm.model as User;
    return this.afAuth.auth.createUserWithEmailAndPassword(model.email, model.password).then((resp) => {
      const userDocRef = this.afStore.doc<User>(`${Fire_Collect.Users}/${resp.user.uid}`)
      return userDocRef.set({ ...model, UID: resp.user.uid }).then((resp) => {
        this.ngProgress.done(); 
        this.toastr.success('Sign up Successfully')
        const title = this.translate.instant('toast.sus');
        this.toastr.success(title)
        this.store.dispatch(new Navigate(['/home']))
      })
    })
  }

  @Action(AuthAction.Login)
  login(ctx: StateContext<AuthStateModel>) {
    this.ngProgress.start();
    const state = ctx.getState();
    const { email, password } = state.loginForm.model;
    return this.afAuth.auth.signInWithEmailAndPassword(email, password).then((resp) => {
      this.ngProgress.done();
      const userId = resp.user.uid;
      const userRef = this.afStore.doc<User>(`${Fire_Collect.Users}/${userId}`);
      return userRef.valueChanges().pipe(
        take(1),
        finalize(() => this.ngProgress.done())
      ).subscribe((resp) => {
        this.ngProgress.done();
        ctx.patchState({
          currentUser: resp,
        })
        const title = this.translate.instant('toast.wtq');
        this.toastr.success(title)
        this.store.dispatch(new Navigate(['/home']))
      })

    }).catch((err) => {
      const state = ctx.getState();
      ctx.patchState({
        loginForm: {
          ...state.loginForm,
          errors: {
            password: err.message,
          }
        }
      })
      this.ngProgress.done()
    })
  }
  @Action(AuthAction.Login)
  subscribe(ctx: StateContext<AuthStateModel>) {

  }


  @Action(AuthAction.UpdateProfile)
  updateProfile(ctx: StateContext<AuthStateModel>) {
    this.ngProgress.start();
    const state = ctx.getState();
    const model = state.signupForm.model as User;
    const address = `${model['street']},${model['postalCode']},${model['city']},${model['country']},`;
    const finalData = {
      ...model,
      address,
    }
    const userDocRef = this.afStore.doc<User>(`${Fire_Collect.Users}/${model.UID}`)
    return userDocRef.update(finalData).then((resp) => {
      this.ngProgress.done();
      this.toastr.success('Profile update successfully');
      const title = this.translate.instant('toast.pus');
      this.toastr.success(title)

      this.store.dispatch(new Navigate(['/home']))
    })
  }
  @Action(AuthAction.SignOut)
  signout(ctx: StateContext<AuthStateModel>) {
    this.ngProgress.start();
    this.afAuth.auth.signOut().then((resp) => {
      this.ngProgress.done();
      ctx.setState({
        ...new AuthStateModel(),
      })
      this.store.dispatch(new Navigate(['login']));
    })
  }
  @Action(AuthAction.FetchUserfeedback)
  fetchUserFeedback(ctx: StateContext<AuthStateModel>, payload: AuthAction.FetchUserfeedback) {
    ctx.patchState({
      selectFeedback: [],
    })
    return this.afStore.collection(Fire_Collect.UserFeedback, (ref) => {
      return ref.where('reviewToId', '==', payload.userid)
    }).valueChanges().pipe(tap((resp: UserFeedback[]) => {
      const users$ = resp.map((item) => {
        return this.afStore.doc(`${Fire_Collect.Users}/${item.reviewFrom}`).snapshotChanges()
      })
      zip(...users$).subscribe((res) => {
        const users = res.map((item) => {
          const id = item.payload.id;
          const user = item.payload.data();
          return {
            id,
            ...user,
          }
        })

        const feedbackList = resp.map((item) => {
          const reviewFromUser = users.find((i) => i.id === item.reviewFrom);
          return {
            ...item,
            reviewFromUser,
          }
        })
        ctx.patchState({
          selectFeedback: feedbackList as UserFeedback[],
        })
      })
    }))
  }
}
