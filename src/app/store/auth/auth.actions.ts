export namespace AuthAction {
  export class Signup {
    static readonly type = '[Auth] sign up';
  }

  export class Login {
    static readonly type = '[Auth] Login'
  }

  export class UpdateProfile {
    static readonly type = '[Auth] profile update'
  }

  export class FetchUserById {
    static readonly type = '[User] fetch user';
    constructor(public payload: string) {
    }
  }

  export class FetchCurrentUser {
    static readonly type = "[User] fetch current User"
  }


  export class UploadImage {
    static readonly type = '[User] Upload User Profile';
    constructor(public payload: any) { }
  }
  export class SignOut {
    static readonly type = '[User] Sign out';
  }

  export class FetchUserfeedback {
    static readonly type = '[user] Fetch User feedback';
    constructor(public userid: string) {

    }
  }
}
