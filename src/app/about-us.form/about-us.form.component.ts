import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';
import { PostState } from 'app/store/post/post.state';

@Component({
  selector: 'app-about-us.form',
  templateUrl: './about-us.form.component.html',
  styleUrls: ['./about-us.form.component.scss']
})
export class AboutUsFormComponent implements OnInit {

  form: FormGroup;
  constructor(private fb: FormBuilder, private store: Store) {
    this.form = this.fb.group({
      introduction: [],
      mission: [],
      vision: [],
    })
  }

  ngOnInit() {
    this.store.dispatch(new PostAction.FetchAboutus());
    this.store.select(PostState.aboutus).subscribe((resp) => {
      if (resp) {
        this.form.patchValue(resp)
      }

    })
  }
  onSubmit() {
    this.store.dispatch(new PostAction.UpdateAboutus());
  }
}
