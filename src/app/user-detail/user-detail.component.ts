import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { AuthState, User } from 'app/store/auth/auth.state';
import { UserFeedback } from 'app/store/post/post.state';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  @Select(AuthState.selectUser)
  user: Observable<User>

  feedback: UserFeedback[] = [];
  constructor(private store: Store) {
    this.store.select(AuthState.selectFeedBack).subscribe((resp) => {
      this.feedback = resp;
    })
  }

  ngOnInit() {
  }

}
