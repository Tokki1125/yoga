import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';
import { Purchases, PostState } from 'app/store/post/post.state';

@Component({
  selector: 'app-my-purchases',
  templateUrl: './my-purchases.component.html',
  styleUrls: ['./my-purchases.component.scss']
})
export class MyPurchasesComponent implements OnInit {
  purchase: Purchases[] = [];
  constructor(private store: Store) {
    this.store.dispatch(new PostAction.FetchMyPurchesing());
    this.store.select(PostState.myPurchaseList).subscribe((resp) => {
      this.purchase = resp;
    })
  }

  ngOnInit() {
  }

}
