import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  isLoaderShow = false;
  constructor() { }

  show() {
    this.isLoaderShow = true;
  }

  hide() {
    this.isLoaderShow = false;
  }
}
