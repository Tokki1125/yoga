import { Component, OnInit } from '@angular/core';
import { View, EventSettingsModel } from '@syncfusion/ej2-schedule';
import { DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService } from '@syncfusion/ej2-angular-schedule';

import { TranslateService } from '@ngx-translate/core';
// import { _ } from '@biesbjerg/ngx-translate-extract/dist/utils/utils';


@Component({
  selector: 'app-calendar1',
  templateUrl: './calendar1.component.html',
  // template: '<ejs-schedule></ejs-schedule>',
  providers: [DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService],
  styleUrls: ['./calendar1.component.scss']
})
export class Calendar1Component implements OnInit {

  constructor(private translate: TranslateService, ) {
    var g_language = window.localStorage.getItem('g_language');
    translate.setDefaultLang(g_language);
  }
  useLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit() {
    // alert(g_language);
  }
  public setView: View = 'Week';
  public setDate: Date = new Date(2019, 11, 17);
  // private eventData: DataManager = new DataManager({
  //   url: 'https://js.syncfusion.com/demos/ejservices/api/Schedule/LoadData',
  //   adaptor: new WebApiAdaptor,
  //   crossDomain: true
  // });

  // public eventObject: EventSettingsModel = {
  //   // dataSource: this.eventData,
  //   dataSource: [
  //     {
  //     EventTitle:"yoga",
  //     EventLocation:"5 members",
  //     EventStart: new Date(2019,12,15,4,0),
  //     EventEnd: new Date(2019,12,16,6,0),

  //     IsBlock: true,
  //     // RecurrenceRule: "FREQ=DAILY;INTERVAL=1;COUNT=10"
  //     IsAllDay:false
  //     } 


  //   ],
  //   fields: {
  //     subject: {name: 'EventTitle', default: "", title: "Enter Title"},
  //     location: {name: 'EventLocation', default: "Number of customers", title: "Number of customers"},
  //     startTime: { name: 'EventStart'},
  //     endTime: {name: 'EventEnd'},
  //     description: {name: 'EventDescription'}
  //   }
  // }
  public data: object[] = [
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 15, 0, 0),
      EndTime: new Date(2019, 11, 15, 23, 59),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 16, 0, 0),
      EndTime: new Date(2019, 11, 16, 7, 30),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'Morning yoga class',
      member: '10 members',
      StartTime: new Date(2019, 11, 16, 10, 0),
      EndTime: new Date(2019, 11, 16, 11, 30),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 16, 12, 0),
      EndTime: new Date(2019, 11, 16, 14, 0),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'body building class',
      StartTime: new Date(2019, 11, 16, 15, 30),
      member: '6 members',
      EndTime: new Date(2019, 11, 16, 17, 30),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 16, 18, 0),
      EndTime: new Date(2019, 11, 16, 23, 59),
      IsBlock: true
    },
    ///////////////////
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 17, 0, 0),
      EndTime: new Date(2019, 11, 17, 7, 30),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'Morning yoga class',
      member: '10 members',
      StartTime: new Date(2019, 11, 17, 9, 30),
      EndTime: new Date(2019, 11, 17, 11, 0),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 17, 12, 0),
      EndTime: new Date(2019, 11, 17, 14, 0),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'body building class',
      member: '10 members',
      StartTime: new Date(2019, 11, 17, 14, 30),
      EndTime: new Date(2019, 11, 17, 17, 30),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 17, 18, 0),
      EndTime: new Date(2019, 11, 17, 23, 59),
      IsBlock: true
    },
    ////////////////
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 18, 0, 0),
      EndTime: new Date(2019, 11, 18, 7, 30),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'Morning yoga class',
      member: '7 members',
      StartTime: new Date(2019, 11, 18, 9, 30),
      EndTime: new Date(2019, 11, 18, 11, 15),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 18, 12, 0),
      EndTime: new Date(2019, 11, 18, 14, 0),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'body building class',
      member: '4 members',
      StartTime: new Date(2019, 11, 18, 15, 0),
      EndTime: new Date(2019, 11, 18, 17, 30),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 18, 18, 0),
      EndTime: new Date(2019, 11, 18, 23, 59),
      IsBlock: true
    },
    //////////
    ////////////////
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 19, 0, 0),
      EndTime: new Date(2019, 11, 19, 7, 30),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'Morning yoga class',
      StartTime: new Date(2019, 11, 19, 10, 0),
      member: '8 members',
      EndTime: new Date(2019, 11, 19, 11, 30),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 19, 12, 0),
      EndTime: new Date(2019, 11, 19, 14, 0),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'body building class',
      member: '3 members',
      StartTime: new Date(2019, 11, 19, 15, 30),
      EndTime: new Date(2019, 11, 19, 17, 30),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 19, 18, 0),
      EndTime: new Date(2019, 11, 19, 23, 59),
      IsBlock: true
    },
    //////////
    ////////////////
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 20, 0, 0),
      EndTime: new Date(2019, 11, 20, 7, 30),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'Morning yoga class',
      member: '2 members',
      StartTime: new Date(2019, 11, 20, 8, 0),
      EndTime: new Date(2019, 11, 20, 9, 30),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 20, 12, 0),
      EndTime: new Date(2019, 11, 20, 14, 0),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'body building class',
      member: '3 members',
      StartTime: new Date(2019, 11, 20, 14, 30),
      EndTime: new Date(2019, 11, 20, 17, 30),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 20, 18, 0),
      EndTime: new Date(2019, 11, 20, 23, 59),
      IsBlock: true
    },
    //////////
    ////////////////
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 21, 0, 0),
      EndTime: new Date(2019, 11, 21, 7, 30),
      IsBlock: true
    },
    {
      Id: 1,
      Subject: 'Morning yoga class',
      member: '5',
      StartTime: new Date(2019, 11, 21, 10, 0),
      EndTime: new Date(2019, 11, 21, 11, 30),
    },
    {
      Id: 1,
      Subject: 'Not available',
      StartTime: new Date(2019, 11, 21, 12, 0),
      EndTime: new Date(2019, 11, 21, 23, 59),
      IsBlock: true
    },
    // {
    //   Id: 1,
    //   Subject: 'body building class: 10 members',
    //   StartTime: new Date(2019, 11, 16, 15, 30),
    //   EndTime: new Date(2019, 11, 16, 17, 30),
    // },
    // {
    //   Id: 1,
    //   Subject: 'Not available',
    //   StartTime: new Date(2019, 11, 16, 18, 0),
    //   EndTime: new Date(2019, 11, 16, 23, 59),
    //   IsBlock: true
    // },
    // //////////







  ];
  public eventSettings: EventSettingsModel = {

    // fields: {
    //   subject: {name: 'EventTitle', default: "", title: "Enter Title"},
    //   location: {name: 'EventLocation', default: "Number of customers", title: "Number of customers"},
    //   startTime: { name: 'EventStart'},
    //   endTime: {name: 'EventEnd'},
    //   description: {name: 'EventDescription'}
    // },
    dataSource: this.data,
    fields: {
      // subject: {name: 'EventTitle', default: "", title: "Enter Title"},
      location: { name: 'member', title: "Number of customers" }
    }
  }
}
