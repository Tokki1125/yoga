import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Calendar1RoutingModule } from './calendar1-routing.module';
import { Calendar1Component } from './calendar1/calendar1.component';
import { ScheduleModule } from '@syncfusion/ej2-angular-schedule';


import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
@NgModule({
  imports: [
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    }),
    CommonModule,
    Calendar1RoutingModule,
    ScheduleModule
  ],
  declarations: [Calendar1Component]
})
export class Calendar1Module { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
