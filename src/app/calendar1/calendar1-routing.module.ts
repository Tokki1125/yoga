import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Calendar1Component } from './calendar1/calendar1.component';

// const routes: Routes = [];
const routes: Routes = [
  {path : 'calendar', component : Calendar1Component}
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Calendar1RoutingModule { }
