import { Component, ViewChild, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { NgProgressService } from './ngProgress/ng-progress.service';
import {TranslateService} from '@ngx-translate/core';
// import {_} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';

// import defaultLanguage from "../assets/i18n/en.json";
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;
  constructor(private translate: TranslateService, private toastrService: ToastrService, public progress: NgProgressService) {
    var g_language = window.localStorage.getItem('g_language');
    translate.setDefaultLang(g_language);
  }
  ngOnInit() {
    this.toastrService.overlayContainer = this.toastContainer;
    
  }
  useLanguage(language: string) {
    this.translate.use(language);
  }
  

}
