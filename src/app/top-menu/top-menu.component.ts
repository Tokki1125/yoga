// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-top-menu',
//   templateUrl: './top-menu.component.html',
//   styleUrls: ['./top-menu.component.scss']
// })
// export class TopMenuComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit } from '@angular/core';
// import { Data, AppService } from '../../../app.service';
import { Data, AppService } from '../app.service';

 
@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {
  public currencies = ['USD', 'EUR'];
  public currency:any;
  public flags = [
    { name:'English', image: 'assets/img/flags/gb.svg' },
    { name:'German', image: 'assets/img/flags/de.svg' },
    { name:'French', image: 'assets/img/flags/fr.svg' },
    { name:'Russian', image: 'assets/img/flags/ru.svg' },
    { name:'Turkish', image: 'assets/img/flags/tr.svg' }
  ]
  public flag:any;

  constructor(public appService:AppService) { }

  ngOnInit() {
    this.currency = this.currencies[0];
    this.flag = this.flags[0];    
  }

  public changeCurrency(currency){
    this.currency = currency;
  }

  public changeLang(flag){
    this.flag = flag;
  }

  

}
