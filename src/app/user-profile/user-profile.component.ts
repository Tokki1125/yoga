import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

// ngxs
import { Store, Select } from '@ngxs/store';
import { AuthState, User } from 'app/store/auth/auth.state';
import { Observable } from 'rxjs';
import { Navigate } from '@ngxs/router-plugin';
import { AuthAction } from 'app/store/auth/auth.actions';
import { AngularFireStorage } from '@angular/fire/storage';
import { map, finalize } from 'rxjs/operators'
import { ActivatedRoute } from '@angular/router';
import { UserFeedback } from 'app/store/post/post.state';
import { NgProgressService } from 'app/ngProgress/ng-progress.service';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, AfterViewInit {

  form: FormGroup;
  @ViewChild('fileInput') fileInput: ElementRef;
  @Select(AuthState.currentUser)
  currentUser: Observable<User>
  feedback: UserFeedback[] = [];
  imagesUrl;
  files: FileList;
  uploadedImagesUrl;
  constructor(
    private fb: FormBuilder,
    private store: Store,
    public afStorage: AngularFireStorage,
    private route: ActivatedRoute,
    private progressBar: NgProgressService,
  ) {
    this.form = this.fb.group({
      email: ['', [Validators.required]],
      image: ['', [Validators.required]],
      name: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      city: ['', [Validators.required]],
      UID: ['', [Validators.required]],
      photoId: [''],
      photoIdType: [],
      status: ['', [Validators.required]],
      postalCode: ['', [Validators.required]],
      street: ['', [Validators.required]],
      country: ['', [Validators.required]],
    })
    const id = this.route.snapshot.paramMap.get('id');

    this.store.dispatch(new AuthAction.FetchCurrentUser());


    this.currentUser.subscribe((resp) => {
      let data;
      if (resp) {
        if (typeof resp.address === 'string') {
          const address = resp.address.split(',');
          const street = address[0];
          const postalCode = address[1];
          const city = address[2];
          const country = address[3];
          data = {
            ...resp,
            street,
            postalCode,
            city,
            country,
          }
        }

        this.form.patchValue(data);
        this.imagesUrl = resp.photoId;
        this.uploadedImagesUrl = this.imagesUrl;
        this.store.dispatch(new AuthAction.FetchUserfeedback(resp.UID))
      }
    });

    this.store.select(AuthState.selectFeedBack).subscribe((resp) => {
      this.feedback = resp;
    })
  }
  get photoId(): FormControl {
    return this.form.get('photoId') as FormControl
  }
  get image(): FormControl {
    return this.form.get('image') as FormControl;
  }
  ngOnInit() {
  }
  uploading: Observable<any>
  isLoaderShow = false;

  onFileUpload(event) {
    if (event.target.files) {
      this.files = event.target.files;

      Object.keys(this.files).forEach((key) => {
        const reader = new FileReader();
        reader.onload = (e) => {
          this.imagesUrl = e.target['result'];

        }
        reader.readAsDataURL(this.files[key])
      })

    }
  }
  onSubmit() {
    if (this.form.invalid) {
      return;
    }
    this.onImagesSaving().then((resp) => {
      this.form.get('photoId').setValue(this.uploadedImagesUrl);
    }).then(() => {
      setTimeout(() => {
        this.store.dispatch(new AuthAction.UpdateProfile());
      }, 1000);
    })
  }
  ngAfterViewInit(): void {

  }
  openDialog() {
    this.fileInput.nativeElement.click();
  }

  uploadProfileImage(event) {
    this.progressBar.start();
    this.isLoaderShow = true;
    const image = event.target.files[0];
    const path = `userprofile/${new Date().getTime()}_${image.name}`
    const fileRef = this.afStorage.ref(path);
    const task = fileRef.put(image);

    this.uploading = task.percentageChanges();
    task.snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe((resp) => {
          this.progressBar.done();
          this.isLoaderShow = false;
          this.image.setValue(resp);
        })
      })
    )
      .subscribe()

  }

  onImagesSaving(): Promise<any> {
    this.progressBar.start();
    return new Promise((resl, rej) => {
      const length = this.files && this.files.length;
      if (!length) {
        resl(this.uploadedImagesUrl);
      }
      let uploadedLength = 0;
      for (const key of Object.keys(this.files)) {
        const path = `userprofile/${new Date().getTime()}_${this.files[key]['name']}`
        const fileRef = this.afStorage.ref(path);
        const task = fileRef.put(this.files[key]);

        this.uploading = task.percentageChanges();
        task.snapshotChanges().pipe(
          finalize(() => {
            fileRef.getDownloadURL().subscribe((resp) => {
              uploadedLength++;

              this.isLoaderShow = false;
              this.uploadedImagesUrl = resp;
              if (uploadedLength === length) {
                resl(this.uploadedImagesUrl);
              }
            })
          })
        )
          .subscribe()
      }
    })

  }
  onRemoveImage(idx) {
    this.uploadedImagesUrl = this.uploadedImagesUrl.filter((item, i) => i !== idx);
    this.imagesUrl = this.uploadedImagesUrl;
    if (this.files) {
      delete this.files[idx];
    }

  }
}
