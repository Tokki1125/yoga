import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AuthAction } from 'app/store/auth/auth.actions';
import { AuthService } from "../services/auth.service";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  form: FormGroup;
  mismatch: string = " ";
  error_messages = {
    'firstname': [
      { type: 'required', message: 'First Name is required.' },
    ],

    'lastname': [
      { type: 'required', message: 'Last Name is required.' }
    ],

    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'required', message: 'please enter a valid email address.' }
    ],

    'password': [
      { type: 'required', message: 'password is required at least 6 charactors.' },
      { type: 'minlength', message: 'at least 6 charactors' },
      { type: 'maxlength', message: 'password length.' }
    ],
    'passwordConfirm': [
      { type: 'required', message: 'password is required at least 6 charactors.' },
      { type: 'minlength', message: 'at least 6 charactors' },
      { type: 'maxlength', message: 'password length.' }
    ],
  }
  flag: boolean;
  constructor(
    public authService: AuthService,
    private router: Router,
    private fb: FormBuilder,
    private store: Store,
    private translate: TranslateService
  ) {
    var g_language = window.localStorage.getItem('g_language');
    translate.setDefaultLang(g_language);
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      // image: ['', []],
      firstname: ['', [Validators.required]],
      lastname: ['', [Validators.required]],
      // phone: ['', [Validators.required]],
      // address: ['', [Validators.required]],
      password: ['', [Validators.required,Validators.minLength(6)]],
      passwordConfirm: ['', [Validators.required,Validators.minLength(6)]],
      
      deviceToken: [null]
    }, { 
      Validators: this.password.bind(this.form)
    })

  }
  useLanguage(language: string) {
    this.translate.use(language);
  }
  btn_signup(){
    console.log("button_clicked");
  }
  password(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: passwordConfirm } = formGroup.get('passwordConfirm');
    return password === passwordConfirm ? null : { passwordNotMatch: true };
  }
  ngOnInit() {
    this.flag = true;
  }
  onSubmit() {
    if (this.form.valid && !this.password(this.form)) {
      this.mismatch = " ";
      this.store.dispatch(new AuthAction.Signup());
    }
    else{
      this.mismatch = "Password mismatched!";
    }

  }
  onshow(){
    this.flag = !this.flag;
  }
}
