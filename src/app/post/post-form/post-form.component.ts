import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store, Select } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { AuthState, User } from 'app/store/auth/auth.state';
import { ActivatedRoute } from '@angular/router';
import { Post, PostState, RateType ,type} from 'app/store/post/post.state';
import { NgProgressService } from 'app/ngProgress/ng-progress.service';
import {TranslateService} from '@ngx-translate/core';
// import {_} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';
import { View, EventSettingsModel } from '@syncfusion/ej2-schedule';
import { DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService, PopupOpenEventArgs} from '@syncfusion/ej2-angular-schedule';
import { createElement } from '@syncfusion/ej2-base';
import { DropDownList } from '@syncfusion/ej2-dropdowns';
import { DateTimePicker } from '@syncfusion/ej2-calendars';
@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss'],
  providers: [DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService],
})
export class PostFormComponent implements OnInit {
  // public views: Array<string> = ['Day', 'Week', 'WorkWeek', 'Month'];

  // onPopupOpen(args: PopupOpenEventArgs): void {
  //      if (args.type === 'Editor') {
  //         // Create required custom elements in initial time
  //         if (!args.element.querySelector('.custom-field-row')) {
  //             let row: HTMLElement = createElement('div', { className: 'custom-field-row' });
  //             let formElement: HTMLElement = args.element.querySelector('.e-schedule-form');
  //             formElement.firstChild.insertBefore(row, args.element.querySelector('.e-title-location-row'));
  //             let container: HTMLElement = createElement('div', { className: 'custom-field-container' });
  //             let inputEle: HTMLInputElement = createElement('input', {
  //                 className: 'e-field', attrs: { name: 'EventType' }
  //             }) as HTMLInputElement;
  //             container.appendChild(inputEle);
  //             row.appendChild(container);
  //             let dropDownList: DropDownList = new DropDownList({
  //                 dataSource: [
  //                     { text: 'Public Event', value: 'public-event' },
  //                     { text: 'Maintenance', value: 'maintenance' },
  //                     { text: 'Commercial Event', value: 'commercial-event' },
  //                     { text: 'Family Event', value: 'family-event' }
  //                 ],
  //                 fields: { text: 'text', value: 'value' },
  //                 value: (<{ [key: string]: Object }>(args.data)).EventType as string,
  //                 floatLabelType: 'Always', placeholder: 'Event Type'
  //             });
  //             dropDownList.appendTo(inputEle);
  //             inputEle.setAttribute('name', 'EventType');
  //         }
  //     }
  // }
  public selectedDate: Date = new Date(2018, 1, 15);
  public views: Array<string> = ['Day', 'Week', 'WorkWeek', 'Month'];
  public showQuickInfo: Boolean = false;
  public eventSettings: EventSettingsModel = {
      // dataSource: eventData
  };
  onPopupOpen(args: PopupOpenEventArgs): void {
      if (args.type === 'Editor') {
          let statusElement: HTMLInputElement = args.element.querySelector('#EventType') as HTMLInputElement;
          if (!statusElement.classList.contains('e-dropdownlist')) {
              let dropDownListObject: DropDownList = new DropDownList({
                  placeholder: 'Choose status', value: statusElement.value,
                  dataSource: ['1', '2', '3', '4','5 to 10', '10+']
              });
              dropDownListObject.appendTo(statusElement);
              statusElement.setAttribute('name', 'EventType');
          }
          let startElement: HTMLInputElement = args.element.querySelector('#StartTime') as HTMLInputElement;
          if (!startElement.classList.contains('e-datetimepicker')) {
              new DateTimePicker({ value: new Date(startElement.value) || new Date() }, startElement);
          }
          let endElement: HTMLInputElement = args.element.querySelector('#EndTime') as HTMLInputElement;
          if (!endElement.classList.contains('e-datetimepicker')) {
              new DateTimePicker({ value: new Date(endElement.value) || new Date() }, endElement);
          }
      }
  }





  form: FormGroup;
  isLoaderShow = false;
  uploading: Observable<number>
  imagesUrl = [];
  rateType = RateType;
  type = type;
  @Select(AuthState.currentUser)
  user$: Observable<User>;
  selectPost: Post;
  id;
  files: FileList;
  uploadedImagesUrl = [];
  constructor(private translate: TranslateService,private progressBar: NgProgressService,
    private _fb: FormBuilder, private store: Store, private afStorage: AngularFireStorage, private route: ActivatedRoute) {
      var g_language = window.localStorage.getItem('g_language');
      translate.setDefaultLang(g_language);
      this.form = this._fb.group({
      userid: [''],
      // type: ['', [Validators.required]],
      title: ['', [Validators.required]],
      status: [1, [Validators.required]],
      rateType: [''],
      type: [''],
      rateValue: ['', [Validators.required]],
      imageList: ['', [Validators.required]],
      id: [],
      description: ['', [Validators.required]],
      date: [this.getDate()],
      city: ['', [Validators.required]],
      postalCode: [],
      street: [],
      country: [],
    })

  }
  useLanguage(language: string) {
    this.translate.use(language);
  }
  getDate() {
    const date = new Date();
    return `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
  }
  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) {
      this.store.dispatch(new PostAction.SelectPost(this.id));
      this.store.select(PostState.selectPost).subscribe((post) => {
        if (post) {
          this.form.patchValue(post);
          this.imagesUrl = post.imageList;
          this.uploadedImagesUrl = [...post.imageList];
        }
      })
    } else {
      // this.user$.subscribe((user) => {
      //   this.form.get('userid').setValue(user.UID);
      // })
    }

  }

  onSubmit() {
    this.onImagesSaving().then((resp) => {
      this.form.get('imageList').setValue(this.uploadedImagesUrl);
      if (this.form.valid) {
        this.progressBar.start();
        setTimeout(() => {
          this.store.dispatch(new PostAction.SavePost());
        }, 1000);
      }
    })


  }

  onFileUpload(event) {
    if (event.target.files) {
      this.files = event.target.files;

      Object.keys(this.files).forEach((key) => {
        const reader = new FileReader();
        reader.onload = (e) => {
          this.imagesUrl.push(e.target['result']);

        }
        reader.readAsDataURL(this.files[key])
      })

    }
    // const image = event.target.files[0];
    // console.log(image);
    // var reader = new FileReader();




  }

  onImagesSaving(): Promise<any> {
    return new Promise((resl, rej) => {
      const length = this.files && this.files.length;
      if (!length) {
        resl(this.uploadedImagesUrl);
      }
      let uploadedLength = 0;
      for (const key of Object.keys(this.files)) {
        const path = `userprofile/${new Date().getTime()}_${this.files[key]['name']}`
        const fileRef = this.afStorage.ref(path);
        const task = fileRef.put(this.files[key]);

        this.uploading = task.percentageChanges();
        task.snapshotChanges().pipe(
          finalize(() => {
            fileRef.getDownloadURL().subscribe((resp) => {
              uploadedLength++;

              this.isLoaderShow = false;
              this.uploadedImagesUrl.push(resp);
              if (uploadedLength === length) {
                resl(this.uploadedImagesUrl);
              }
            })
          })
        )
          .subscribe()
      }
    })


  }
  onRemoveImage(idx) {
    this.uploadedImagesUrl = this.uploadedImagesUrl.filter((item, i) => i !== idx);
    this.imagesUrl = this.uploadedImagesUrl;
    if (this.files) {
      delete this.files[idx];
    }

  }
  

  public setView: View = 'Month';
  public setDate: Date = new Date(2020, 1, 11);
  // public data: object[] = [
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 15, 0, 0),
  //     EndTime: new Date(2019, 11, 15, 23, 59),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 16, 0, 0),
  //     EndTime: new Date(2019, 11, 16, 7, 30),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Morning yoga class',
  //     member: '10 members',
  //     StartTime: new Date(2019, 11, 16, 10, 0),
  //     EndTime: new Date(2019, 11, 16, 11, 30),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 16, 12, 0),
  //     EndTime: new Date(2019, 11, 16, 14, 0),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'body building class',
  //     StartTime: new Date(2019, 11, 16, 15, 30),
  //     member: '6 members',
  //     EndTime: new Date(2019, 11, 16, 17, 30),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 16, 18, 0),
  //     EndTime: new Date(2019, 11, 16, 23, 59),
  //     IsBlock: true
  //   },
  //   ///////////////////
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 17, 0, 0),
  //     EndTime: new Date(2019, 11, 17, 7, 30),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Morning yoga class',
  //     member: '10 members',
  //     StartTime: new Date(2019, 11, 17, 9, 30),
  //     EndTime: new Date(2019, 11, 17, 11, 0),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 17, 12, 0),
  //     EndTime: new Date(2019, 11, 17, 14, 0),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'body building class',
  //     member: '10 members',
  //     StartTime: new Date(2019, 11, 17, 14, 30),
  //     EndTime: new Date(2019, 11, 17, 17, 30),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 17, 18, 0),
  //     EndTime: new Date(2019, 11, 17, 23, 59),
  //     IsBlock: true
  //   },
  //   ////////////////
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 18, 0, 0),
  //     EndTime: new Date(2019, 11, 18, 7, 30),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Morning yoga class',
  //     member: '7 members',
  //     StartTime: new Date(2019, 11, 18, 9, 30),
  //     EndTime: new Date(2019, 11, 18, 11, 15),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 18, 12, 0),
  //     EndTime: new Date(2019, 11, 18, 14, 0),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'body building class',
  //     member: '4 members',
  //     StartTime: new Date(2019, 11, 18, 15, 0),
  //     EndTime: new Date(2019, 11, 18, 17, 30),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 18, 18, 0),
  //     EndTime: new Date(2019, 11, 18, 23, 59),
  //     IsBlock: true
  //   },
  //   //////////
  //   ////////////////
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 19, 0, 0),
  //     EndTime: new Date(2019, 11, 19, 7, 30),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Morning yoga class',
  //     StartTime: new Date(2019, 11, 19, 10, 0),
  //     member: '8 members',
  //     EndTime: new Date(2019, 11, 19, 11, 30),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 19, 12, 0),
  //     EndTime: new Date(2019, 11, 19, 14, 0),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'body building class',
  //     member: '3 members',
  //     StartTime: new Date(2019, 11, 19, 15, 30),
  //     EndTime: new Date(2019, 11, 19, 17, 30),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 19, 18, 0),
  //     EndTime: new Date(2019, 11, 19, 23, 59),
  //     IsBlock: true
  //   },
  //   //////////
  //   ////////////////
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 20, 0, 0),
  //     EndTime: new Date(2019, 11, 20, 7, 30),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Morning yoga class',
  //     member: '2 members',
  //     StartTime: new Date(2019, 11, 20, 8, 0),
  //     EndTime: new Date(2019, 11, 20, 9, 30),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 20, 12, 0),
  //     EndTime: new Date(2019, 11, 20, 14, 0),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'body building class',
  //     member: '3 members',
  //     StartTime: new Date(2019, 11, 20, 14, 30),
  //     EndTime: new Date(2019, 11, 20, 17, 30),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 20, 18, 0),
  //     EndTime: new Date(2019, 11, 20, 23, 59),
  //     IsBlock: true
  //   },
  //   //////////
  //   ////////////////
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 21, 0, 0),
  //     EndTime: new Date(2019, 11, 21, 7, 30),
  //     IsBlock: true
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Morning yoga class',
  //     member: '5',
  //     StartTime: new Date(2019, 11, 21, 10, 0),
  //     EndTime: new Date(2019, 11, 21, 11, 30),
  //   },
  //   {
  //     Id: 1,
  //     Subject: 'Not available',
  //     StartTime: new Date(2019, 11, 21, 12, 0),
  //     EndTime: new Date(2019, 11, 21, 23, 59),
  //     IsBlock: true
  //   },
  //   // {
  //   //   Id: 1,
  //   //   Subject: 'body building class: 10 members',
  //   //   StartTime: new Date(2019, 11, 16, 15, 30),
  //   //   EndTime: new Date(2019, 11, 16, 17, 30),
  //   // },
  //   // {
  //   //   Id: 1,
  //   //   Subject: 'Not available',
  //   //   StartTime: new Date(2019, 11, 16, 18, 0),
  //   //   EndTime: new Date(2019, 11, 16, 23, 59),
  //   //   IsBlock: true
  //   // },
  //   // //////////







  // ];
  // public eventSettings: EventSettingsModel = {

  //   // fields: {
  //   //   subject: {name: 'EventTitle', default: "", title: "Enter Title"},
  //   //   location: {name: 'EventLocation', default: "Number of customers", title: "Number of customers"},
  //   //   startTime: { name: 'EventStart'},
  //   //   endTime: {name: 'EventEnd'},
  //   //   description: {name: 'EventDescription'}
  //   // },
  //   // dataSource: this.data,
  //   fields: {
  //     // subject: {name: 'EventTitle', default: "", title: "Enter Title"},
  //     location: { name: 'member', title: "Number of customers" }
  //   }
  // }
}
