import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Blog, PostState } from 'app/store/post/post.state';
import { PostAction } from 'app/store/post/post.actions';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss']
})
export class BlogListComponent implements OnInit {
  blogs: Blog[] = [];
  constructor(private store: Store) { }

  ngOnInit() {
    this.store.dispatch(new PostAction.FetchBlog());
    this.store.select(PostState.blogList).subscribe((resp) => {
      this.blogs = resp;
    })
  }

}
