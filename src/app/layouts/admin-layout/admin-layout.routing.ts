import { LoginComponent } from 'app/login/login.component';
import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { SignUpComponent } from 'app/sign-up/sign-up.component';
import { LandingComponent } from 'app/public-pages/landing/landing.component';
import { DetailComponent } from 'app/public-pages/detail/detail.component';
import { ChatPageComponent } from 'app/chat-page/chat-page.component';
import { PostFormComponent } from 'app/post/post-form/post-form.component';
import { UserDetailComponent } from 'app/user-detail/user-detail.component';
import { MyPostComponent } from 'app/my-post/my-post.component';
import { MyPurchasesComponent } from 'app/my-purchases/my-purchases.component';
import { AboutUsComponent } from 'app/about-us/about-us.component';
import { HowWorksComponent } from 'app/how-works/how-works.component';
import { ContactUsComponent } from 'app/contact-us/contact-us.component';
import { BlogFormComponent } from 'app/blog-form/blog-form.component';
import { ContactListComponent } from 'app/contact-list/contact-list.component';
import { BlogListComponent } from 'app/blog-list/blog-list.component';
import { ContactFormComponent } from 'app/contact-form/contact-form.component';
import { AboutUsFormComponent } from 'app/about-us.form/about-us.form.component';
import { PartnersComponent } from 'app/partners/partners.component';
import { Calendar1Component } from 'app/calendar1/calendar1/calendar1.component';
import { ResetpwdComponent } from 'app/resetpwd/resetpwd.component';
export const AdminLayoutRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'sign-up', component: SignUpComponent },
    { path: 'resetpwd', component: ResetpwdComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'user-profile', component: UserProfileComponent },
    { path: 'post', component: PostFormComponent },
    { path: 'table-list', component: TableListComponent },
    { path: 'typography', component: TypographyComponent },
    { path: 'icons', component: IconsComponent },
    { path: 'maps', component: MapsComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'upgrade', component: UpgradeComponent },
    { path: 'chat-page', component: ChatPageComponent },
    {
        path: 'home',
        component: LandingComponent,
    },
    {
        path: 'detail',
        component: DetailComponent,
    }, {

        path: 'detail/:id',
        component: DetailComponent,
    },
    {
        path: 'post/:id',
        component: PostFormComponent,
    },
    {
        path: 'my-post',
        component: MyPostComponent,
    },
    {
        path: 'my-purchases',
        component: MyPurchasesComponent,
    },
    {
        path: 'user-detail/:id',
        component: UserDetailComponent,
    },
    {
        path: 'about-us',
        component: AboutUsComponent,
    },
    {
        path: 'how-works',
        component: HowWorksComponent,
    },
    {
        path: 'contact-us',
        component: ContactUsComponent,
    },
    {
        path: 'add-blog',
        component: BlogFormComponent,
    },
    {
        path: 'contact-list',
        component: ContactListComponent,
    },
    {
        path: 'blog-list',
        component: BlogListComponent,
    },
    {
        path: 'update-contact',
        component: ContactFormComponent,
    },
    {
        path: 'update-aboutus',
        component: AboutUsFormComponent,
    },
    {
        path: 'partners',
        component: PartnersComponent,
    },
    {
        path: 'calendar',
        component: Calendar1Component,
    }
    
];
