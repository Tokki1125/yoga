import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy, PopStateEvent } from '@angular/common';
import 'rxjs/add/operator/filter';
import { NavbarComponent } from '../../components/navbar/navbar.component';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Store } from '@ngxs/store';
import { AuthAction } from 'app/store/auth/auth.actions';
import { AuthState } from 'app/store/auth/auth.state';
import { AngularFireAuth } from '@angular/fire/auth';
import { PostAction } from 'app/store/post/post.actions';
import { PostState } from 'app/store/post/post.state';
import { MatDialog } from '@angular/material';
import { FeedBackComponent } from 'app/feed-back/feed-back.component';
import { take } from 'rxjs/operators';
import { LoaderService } from 'app/loader.service';

declare const $;
@Component({
    selector: 'app-admin-layout',
    templateUrl: './admin-layout.component.html',
    styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {
    currentUser
    constructor(
        private dialog: MatDialog,
        private afAuth: AngularFireAuth,
        public location: Location,
        private router: Router,
        private store: Store,
        public loader: LoaderService) {
        this.store.dispatch(new AuthAction.FetchCurrentUser());
        this.store.select(AuthState.currentUser).subscribe((resp) => {
            this.currentUser = resp;
        })
    }

    ngOnInit() {
        this.afAuth.authState.subscribe((resp) => {
            if (resp) {
                this.store.dispatch(new PostAction.FetchUnRatePurches())
            }
        })
        this.store.select(PostState.unRatePurchaseList).subscribe((resp) => {
            if (Array.isArray(resp) && resp.length) {
                const data = resp[0];
                this.store.dispatch(new AuthAction.FetchUserById(data.userid));
                const dRef = this.dialog.open(FeedBackComponent, {
                    data: {
                        name: data.title,
                        title: 'Rate Post',
                    }
                });
                dRef.afterClosed().subscribe((feedback) => {
                    this.store.dispatch(new PostAction.RatePost({
                        postId: data.id,
                        postRating: feedback.rate,
                        postReview: feedback.review,
                    }))
                    const user = this.store.selectSnapshot(AuthState.selectUser);
                    const duRef = this.dialog.open(FeedBackComponent, {
                        data: {
                            name: user.name,
                            title: 'Rate User'
                        }
                    })
                    duRef.afterClosed().subscribe((feedb) => {
                        this.store.dispatch(new PostAction.RateUser({
                            postId: data.id,
                            rating: feedb.rate,
                            review: feedb.review,
                            reviewToId: data.userid,
                        }))

                        this.store.dispatch(new PostAction.RateComplete(data.purId));
                    })

                })
            }
        })
    }

    get isLogin() {
        return this.currentUser;
    }
    isMaps(path) {
        var titlee = this.location.prepareExternalUrl(this.location.path());

        titlee = titlee.slice(1);
        if (path == titlee) {
            return false;
        }
        else {
            return true;
        }
    }
    get isNavShow() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        titlee = titlee.slice(1);
        return true;
    }
    get isSidebar() {
        var titlee = this.location.prepareExternalUrl(this.location.path());

        titlee = titlee.slice(1);
        if (this.isMobileMenu()) {
            return true;
        }
        if (
            titlee.includes('partners') ||
            titlee.includes('about-us') ||
            titlee.includes('contact-us') ||
            titlee.includes('blog-list') ||
            titlee.includes('chat-page') ||
            'home' === titlee ||
            titlee.includes('detail') ||
            'login' === titlee ||
            'sign-up' === titlee
        ) {
            return false;
        } else {
            return true;
        }
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };
}
