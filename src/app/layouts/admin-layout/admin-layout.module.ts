import { ScheduleModule } from '@syncfusion/ej2-angular-schedule';
import { NgModule } from '@angular/core'; 
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import { SignUpComponent } from 'app/sign-up/sign-up.component';
import { LoginComponent } from 'app/login/login.component';
import { ChatPageComponent } from './../../chat-page/chat-page.component';
import { TopMenuComponent } from '../../top-menu/top-menu.component';
import {
  AgmCoreModule
} from '@agm/core';

import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { ResetpwdComponent } from 'app/resetpwd/resetpwd.component';


import {
  
  
  MatButtonModule,
  MatInputModule,
  MatRippleModule,
  MatFormFieldModule,
  MatTooltipModule,
  MatSelectModule,
  MatProgressSpinnerModule,
  MatDatepickerModule,
  MatIconModule,
  MatRadioModule,
  MatNativeDateModule,

} from '@angular/material';
import { MatListModule } from '@angular/material/list'
import { NgxsModule } from '@ngxs/store';
import { AuthState } from 'app/store/auth/auth.state';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { LandingComponent } from 'app/public-pages/landing/landing.component';

import { DetailComponent } from 'app/public-pages/detail/detail.component';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { PostFormComponent } from 'app/post/post-form/post-form.component';
import { PostState } from 'app/store/post/post.state';
import { UserDetailComponent } from 'app/user-detail/user-detail.component';
import { MyPostComponent } from 'app/my-post/my-post.component';
import { MyPurchasesComponent } from 'app/my-purchases/my-purchases.component';
import { AboutUsComponent } from 'app/about-us/about-us.component';
import { HowWorksComponent } from 'app/how-works/how-works.component';
import { ContactUsComponent } from 'app/contact-us/contact-us.component';
import { BlogFormComponent } from 'app/blog-form/blog-form.component';
import { ContactListComponent } from 'app/contact-list/contact-list.component';
import { BlogListComponent } from 'app/blog-list/blog-list.component';
import { ContactFormComponent } from 'app/contact-form/contact-form.component';
import { AboutUsFormComponent } from 'app/about-us.form/about-us.form.component';
import { PartnersComponent } from 'app/partners/partners.component';
import { Calendar1Module } from 'app/calendar1/calendar1.module';
@NgModule({
  imports: [
    ScheduleModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBNcjxo_35qnEG17dQvvftWa68eZWepYE0'
    }),
    HttpClientModule,
    TranslateModule.forRoot({
        loader: {
            provide: TranslateLoader,
            useFactory: HttpLoaderFactory,
            deps: [HttpClient]
        }
    }),



    ReactiveFormsModule,
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    NgxsModule.forFeature([PostState, AuthState]),
    NgxsRouterPluginModule,
    NgxsFormPluginModule,
    FormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatListModule,
    MatIconModule,
    // MatDialogModule,
    MatRadioModule,
    Calendar1Module,
    //firebase

    AngularFireStorageModule,
  ],
  declarations: [
    // TopMenuComponent,

    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    SignUpComponent,
    LoginComponent,
    LandingComponent,
    DetailComponent,
    ChatPageComponent,
    PostFormComponent,
    UserDetailComponent,
    MyPostComponent,
    MyPurchasesComponent,
    AboutUsComponent,
    HowWorksComponent,
    ContactUsComponent,
    BlogFormComponent,
    ContactListComponent,
    BlogListComponent,
    ContactFormComponent,
    AboutUsFormComponent,
    PartnersComponent,
    ResetpwdComponent,
    // Calendar1Component
    // ConfirmDailogComponent,
  ],
  // entryComponents: [ConfirmDailogComponent]
})

export class AdminLayoutModule { }
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}