// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-resetpwd',
//   templateUrl: './resetpwd.component.html',
//   styleUrls: ['./resetpwd.component.scss']
// })
// export class ResetpwdComponent implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { Store } from '@ngxs/store';
import { AuthAction } from 'app/store/auth/auth.actions';
import { ErrorStateMatcher } from '@angular/material/core';
import { AuthState } from 'app/store/auth/auth.state';
import {TranslateService} from '@ngx-translate/core';
// import {_} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';
import { AuthService } from "../../app/services/auth.service";
@Component({
  selector: 'app-resetpwd',
  templateUrl: './resetpwd.component.html',
  styleUrls: ['./resetpwd.component.scss']
}) 
export class ResetpwdComponent implements OnInit {

  form: FormGroup;
  error;
  constructor(public authService: AuthService,private translate: TranslateService,private fb: FormBuilder, private store: Store) {
    
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      // password: ['', [Validators.required]],
    })
    var g_language = window.localStorage.getItem('g_language');
    translate.setDefaultLang(g_language);
  }

  ngOnInit() {
    this.store.select(AuthState.loginError).subscribe((err) => {
      this.error = err;
    })
  }
  useLanguage(language: string) {
    this.translate.use(language);
  }

  onSubmit() {
    // if (this.form.valid) {
    //   this.store.dispatch(new AuthAction.Login())
    // }
    console.log(1)
  }
}

