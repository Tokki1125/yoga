import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';
import { PostState } from 'app/store/post/post.state';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {
  form: FormGroup;
  constructor(private fb: FormBuilder, private store: Store) {
    this.form = this.fb.group({
      phone: [],
      email: [],
      address: [],
    })
  }

  ngOnInit() {
    this.store.dispatch(new PostAction.fetchContact());
    this.store.select(PostState.contact).subscribe((resp) => {
      if(resp){
        this.form.patchValue(resp)
      }
     
    })
  }
  onSubmit() {
    this.store.dispatch(new PostAction.updateContact());
  }
}
