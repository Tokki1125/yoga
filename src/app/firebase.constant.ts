export const Fire_Collect = {
    Users: 'Users',
    Posts: 'Posts',
    TransactionRecord: 'TransactionRecord',
    PostFeedback: 'PostFeedback',
    UserFeedback: 'UserFeedback',
    Contactus: 'ContactUs',
    Blog: 'Blog'
}