import { Component, OnInit, HostListener } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';
import { Observable } from 'rxjs';
import { Post, PostState } from 'app/store/post/post.state';
import { FormGroup, FormBuilder, Validators , FormControl} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
// import {_} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';
import { ToastrService } from 'ngx-toastr';
import { AngularFireAuth } from '@angular/fire/auth';
@HostListener('window:scroll', ['$event']) 
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  test : Date = new Date();
  @Select(PostState.posts)
  postList$: Observable<Post[]>
  postList: Post[] = [];
  filterPostList: Post[] = []
  form: FormGroup;
  form1: FormGroup;

  isMessageShow = false;
  constructor(private fireAuth: AngularFireAuth,private toastr: ToastrService,private translate: TranslateService, private store: Store, private _fb: FormBuilder,) {
    this.form1 = this._fb.group({
      email: ['', [Validators.required, Validators.email]],
    })
    var g_language = window.localStorage.getItem('g_language');
    translate.setDefaultLang(g_language);
    this.store.dispatch(new PostAction.FetchAll());
    this.postList$.subscribe((resp) => {
      if (resp) {
        this.postList = resp
        this.filterPostList = resp;
      }
    })
    this.form = this._fb.group({
      title: [],
      city: [],
      date: [],
    })
  } 
  get isLogin() {
    return this.fireAuth.auth.currentUser;
    // return false;
  }
  useLanguage(language: string) {
    this.translate.use(language);
  }
  onsubscribe(){
    if (this.form1.valid) {
    this.toastr.success('Subscribe success!');
    }

  }
  ngOnInit() {
    if(this.language1 == null){
      if(window.localStorage.getItem('g_language') === 'en'){
        this.language1 = 'English';
        this.language2 = 'Deutsche';
      }
      else{
        this.language1 = 'Deutsche';
        this.language2 = 'English';
      }
    }
  }
  onFilter() {
    this.isMessageShow = true;
    console.log(this.form.value);
    let stDate = '';
    const { title, city, date } = this.form.value;
    if (date) {
      stDate = `${date.getMonth() + 1}/${date.getDate()}/${date.getFullYear()}`;
    }
    this.filterPostList = this.postList.filter((item) => {
      // console.log(item.date === stDate,
      //   item.title.toLowerCase().includes(title),
      //   item.city.toLowerCase().includes(city.toLowerCase()))
      console.log(item.date, stDate)
      return item.date === stDate ||
        item.title.toLowerCase().includes(title ? title.toLowerCase() : title) ||
        item.city.toLowerCase().includes(city ? city.toLowerCase() : city);
    })

    console.log(this.filterPostList);
    window.scroll({ 
      top: 1500, 
      left: 0, 
      behavior: 'smooth' 
    });
  }
  language1:string;
  language2:string;
  public changeLanguage(): void {
    var l = window.localStorage.getItem('g_language');
    if (l === 'en') {
      window.localStorage.setItem('g_language', 'de');
      this.language1 = 'English';
      this.language2 = 'German';
    } else {
      window.localStorage.setItem('g_language', 'en');
      this.language1 = 'German';
      this.language2 = 'English';
    }
    location.reload();
  }
  yoga() {
    // console.debug("Scroll Event", document.body.scrollTop);
    // see András Szepesházi's comment below
    console.log("Scroll Event", window.pageYOffset );
    window.scroll({
      top: 1650, 
      left: 0, 
      behavior: 'smooth'
    })
  }
  pilates(){
    window.scroll({
      top: 1950, 
      left: 0, 
      behavior: 'smooth'
    })
  }
  martial_arts(){
    window.scroll({
      top: 2800, 
      left: 0, 
      behavior: 'smooth'
    })
  }
  dance(){
    window.scroll({
      top: 1350, 
      left: 0, 
      behavior: 'smooth'
    })
  }
  fitness(){
    window.scroll({
      top: 3900, 
      left: 0, 
      behavior: 'smooth'
    })
  }
  massage(){
    window.scroll({
      top: 3150, 
      left: 0, 
      behavior: 'smooth'
    })
  }
}
