import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { ActivatedRoute } from '@angular/router';
import { PostAction } from 'app/store/post/post.actions';
import { Observable, Subject } from 'rxjs';
import { PostState, Post, Chat, PostFeedback, RateType } from 'app/store/post/post.state';
import { AuthState, User } from 'app/store/auth/auth.state';
import { FormControl } from '@angular/forms';
import { AuthAction } from 'app/store/auth/auth.actions';
import { takeUntil, take } from 'rxjs/operators';
import { FirebaseAuth } from '@angular/fire';
import { AngularFireAuth } from '@angular/fire/auth';
import { NgProgressService } from 'app/ngProgress/ng-progress.service';
import { View, EventSettingsModel,ResourcesModel} from '@syncfusion/ej2-schedule';
import { DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService, PopupOpenEventArgs } from '@syncfusion/ej2-angular-schedule';
import { DropDownList } from '@syncfusion/ej2-dropdowns';
import { DateTimePicker } from '@syncfusion/ej2-calendars';
declare let paypal: any;
declare let $;
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  providers: [DayService, WeekService, WorkWeekService, MonthService, AgendaService, MonthAgendaService, TimelineViewsService, TimelineMonthService],
})
export class DetailComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('scheduleObj')
  public selectedDate: Date = new Date(2018, 1, 15);
  public views: Array<string> = ['Day', 'Week', 'WorkWeek', 'Month'];
  public showQuickInfo: Boolean = false;
  private startTime: any;
  private resourcesModel: ResourcesModel;
  private endTime: any;
  public eventSettings: EventSettingsModel;
  public dmin : number;
  // Wed Feb 12 2020 00:00:00 GMT+0800 (China Standard Time)
  onDataBinding(e: { [key: string]: any }): void {
    this.dmin = 0;
    for (let i: number = 0; i < e.result.length; i++) {
      var sdate = new Date(e.result[i].StartTime);
      var edate = new Date(e.result[i].EndTime);
      this.dmin = this.dmin + (edate.getTime()-sdate.getTime())/1000/60/60;
    }
    console.log(this.dmin)
    this.totalPrice = this.dmin * this.post.rateValue;

  }
  onPopupOpen(args: PopupOpenEventArgs): void {


      if (args.type === 'Editor') {
          let statusElement: HTMLInputElement = args.element.querySelector('#EventType') as HTMLInputElement;
          if (!statusElement.classList.contains('e-dropdownlist')) {
              let dropDownListObject: DropDownList = new DropDownList({
                  placeholder: 'Select Number', value: statusElement.value,
                  dataSource: ['1', '2', '3', '4','5 to 10', '10+']
              });
              dropDownListObject.appendTo(statusElement);
              statusElement.setAttribute('name', 'EventType');
          }
          let startElement: HTMLInputElement = args.element.querySelector('#StartTime') as HTMLInputElement;
          if (!startElement.classList.contains('e-datetimepicker')) {
              new DateTimePicker({ value: new Date(startElement.value) || new Date() }, startElement);
          }
          let endElement: HTMLInputElement = args.element.querySelector('#EndTime') as HTMLInputElement;
          if (!endElement.classList.contains('e-datetimepicker')) {
              new DateTimePicker({ value: new Date(endElement.value) || new Date() }, endElement);
          }
      }
  }


  calc_time(){
    console.log(this.eventSettings);

  }
  public lat: number = 40.678178;
  public lng: number = -73.944158;
  public zoom: number = 12;
  @ViewChild('scrolEndRef') scrolSpanRef: ElementRef;

  post: Post;
  feedbacks: PostFeedback[] = [];
  isViewerShow = false;
  selectedImageUrl;
  currentUser: User;
  selectIndex;
  startDate: FormControl = new FormControl();
  endDate = new FormControl();
  totalPrice = this.dmin;
  unSub = new Subject<boolean>();
  isChatBoxShow = false;
  messageControl = new FormControl();
  chat: Chat[];
  // paypal script
  addScript: boolean = false;
  totalDays: number;
  lastMessage: Chat;
  rateType = RateType;
  isLoginMessage = false;
  isDailogShow = true;
  paypalConfig = {
    env: 'sandbox',
    client: {
      sandbox: 'AXtLsK1KX5uEVvuSsCplros2P8gjXIzwMthNkM-CkvnQctcoS2hk4UhoNdgwC7QJP_qMQD9sm-0F8jAT',
      production: 'AXtLsK1KX5uEVvuSsCplros2P8gjXIzwMthNkM-CkvnQctcoS2hk4UhoNdgwC7QJP_qMQD9sm-0F8jAT'
    },
    commit: true,
    language_code: "en_US",
    locale: 'en_US',
    payment: (data, actions) => {
      if (!this.currentUser) {
        this.isLoginMessage = true;
        actions();
      }
      return actions.payment.create({
        payment: {
          transactions: [
            { amount: { total: this.totalPrice, currency: 'EUR' } }
          ]
        }
      });
    },
    onAuthorize: (data, actions) => {
      return actions.payment.execute().then((payment) => {
        //Do something when payment is successful.
        // const start: Date = new Date(this.startDate.value);
        // const end: Date = new Date(this.endDate.value);
        const start: Date = new Date(this.startDate.value);
        const end: Date = new Date(this.endDate.value);
        this.store.dispatch(new PostAction.Purchesing({
          startDate: start.getTime(),
          endDate: end.getTime(),
          postId: this.post.id,
          addedDateTime: new Date().getTime(),
          reviewGiven: false,
          totalCost: this.totalPrice,
          totalDays: this.totalDays,
          transactionId: payment.id,
        }))
      })
    }
  };
  // end of paypal
  constructor(
    private store: Store,
    private activedRoute: ActivatedRoute,
    private fireAuth: AngularFireAuth,
    public progress: NgProgressService,
  ) {
    const id = this.activedRoute.snapshot.paramMap.get('id');
    this.store.dispatch(new PostAction.SelectPost(id));
    this.store.select(PostState.selectPost).pipe(takeUntil(this.unSub)).subscribe((post) => {
      this.post = post;
      if (this.post && this.post.userid) {
        this.store.dispatch(new AuthAction.FetchUserById(this.post.userid))
        this.store.dispatch(new PostAction.CheckForKeyLastMessage(this.post.userid));
        // this.store.dispatch(new PostAction.FetchSelectedChat(this.post.userid));
        this.store.dispatch(new AuthAction.FetchUserfeedback(this.post.userid));
        this.store.dispatch(new PostAction.FetchFeedback(this.post.id));
      }
    })
    this.store.select(PostState.checkLastMessage).subscribe((resp) => {
      if (this.post && resp) {
        this.lastMessage = resp;
        this.store.dispatch(new PostAction.FetchSelectedChat({ id: this.post.userid, lastMessageId: resp.id }))

      }
    })
    this.store.select(AuthState.currentUser).pipe(takeUntil(this.unSub)).subscribe((user) => {
      this.currentUser = user;
    })
    this.startDate.valueChanges.subscribe((resp) => {
      this.calculateDuration();

    })
    this.endDate.valueChanges.subscribe((resp) => {
      if (this.isDailogShow) {
        this.progress.showConfirmDialog();
      }
      this.calculateDuration();
      console.log('hello');
      this.isDailogShow = false;
    })

    this.store.select(PostState.selectChat).subscribe((chat) => {
      this.chat = chat;
      setTimeout(() => {
        this.scrolSpanRef.nativeElement.scrollIntoView();
      }, 100);
    })

    this.store.select(PostState.selectPostFeedbacks).subscribe((resp) => {
      this.feedbacks = resp;
    })

    // this.endDate.valueChanges.subscribe((resp) => {
    //   console.log('hello')
    //   // this.progress.isYesClick$.asObservable().pipe(take(1)).subscribe(() => {

    //   // })
    // })
  }

  get isLogin() {
    return this.fireAuth.auth.currentUser;
  }
  calculateDuration() {
    const start = this.startDate.value;
    const end = this.endDate.value;
    if (!start || !end) {
      return;
    }
    const startDate = new Date(start).getTime();
    const endDate = new Date(end).getTime();
    if (endDate >= startDate) {
      const diff = endDate - startDate;
      if (this.post.rateType === RateType.ratePerHour) {
        this.totalDays = diff / (1000 * 3600)

      } else {
        this.totalDays = Math.ceil(diff / (1000 * 3600 * 24))

      }
      if (this.totalDays) {
        this.totalPrice = this.totalDays * this.post.rateValue;
        if (!Number.isNaN(this.totalPrice)) {
          this.totalPrice = +this.totalPrice.toFixed(2);
        }
      }
    }
  }
  onChangeEndDate() {
    this.calculateDuration();
  }
  get canEdit() {
    if (this.currentUser && this.post) {
      return this.currentUser.UID === this.post.userid;
    }
    return false;
  }
  ngOnInit() {
    window.scrollTo(0, 0);
  }




  public setView: View = 'Month';
  public setDate: Date = new Date(2020, 1, 11);






  ngAfterViewInit(): void {
    setTimeout(() => {
      if (!this.addScript) {
        this.addPaypalScript().then(() => {
          paypal.Button.render(this.paypalConfig, '#paypal-checkout-btn');
        })
      }
    }, 2000);

  }
  addPaypalScript() {
    this.addScript = true;
    return new Promise((resolve, reject) => {
      let scripttagElement = document.createElement('script');
      scripttagElement.src = 'https://www.paypalobjects.com/api/checkout.js';
      scripttagElement.onload = resolve;
      document.body.appendChild(scripttagElement);
    })
  }
  onHide() {
    this.isViewerShow = false;
  }

  onShow(idx) {
    this.selectIndex = idx;
    this.selectedImageUrl = this.post.imageList[idx];
    this.isViewerShow = true;
  }

  move(step) {
    let newIdx;
    const length = this.post.imageList.length;
    if (step === -1) {
      newIdx = this.selectIndex - 1;
    } else {
      newIdx = this.selectIndex + 1;
    }
    if (newIdx === length) {
      newIdx = 0;
    }
    if (newIdx === -1) {
      newIdx = length - 1;
    }
    this.onShow(newIdx)
  }

  ngOnDestroy(): void {
    this.unSub.next();
    this.unSub.complete();
  }

  toggleChatBox() {
    this.isChatBoxShow = !this.isChatBoxShow;
  }
  // send message
  onSendMessage() {
    let lastId = null;
    const value = this.messageControl.value;
    if (this.lastMessage) {
      lastId = this.lastMessage.id;
    }

    this.store.dispatch(new PostAction.SendMessage({
      message: value,
      to: this.post.user.UID,
      lastMessageId: lastId,
    }))
    this.messageControl.setValue('');
  }

  onDelete() {
    this.store.dispatch(new PostAction.DeletePost(this.post.id));
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
