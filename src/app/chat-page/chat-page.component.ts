import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';
import { PostState, Chat } from 'app/store/post/post.state';
import { FormControl } from '@angular/forms';
import { User } from 'app/store/auth/auth.state';
import { AngularFireAuth } from '@angular/fire/auth';
export interface Section {
  name: string;
  updated: string;
}
@Component({
  selector: 'app-chat-page',
  templateUrl: './chat-page.component.html',
  styleUrls: ['./chat-page.component.scss']
})
export class ChatPageComponent implements OnInit {
  lastChatList: Chat[] = [];
  selectChat: Chat[] = [];
  message = new FormControl();
  otherUser: User;
  currentUser;
  onClickSelectChat: Chat;
  @ViewChild('scrolEndRef') scrolSpanRef: ElementRef;
  constructor(private store: Store, private afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe((data) => {
      this.currentUser = data;
    });
    this.store.dispatch(new PostAction.FetchAllChat());
    this.store.select(PostState.lastChatList).subscribe((list) => {
      this.lastChatList = list;
    })
    this.store.select(PostState.selectChat).subscribe((list) => {
      this.selectChat = list;
      setTimeout(() => {
        this.scrolSpanRef.nativeElement.scrollIntoView();
      }, 100);
    })
  }

  ngOnInit() {
  }
  onSelectChat(chat: Chat) {
    this.onClickSelectChat = chat;
    this.store.dispatch(new PostAction.FetchSelectedChat({ id: chat.user.id, lastMessageId: chat.id }))
  }

  get antherUserId(): string {
    const chat = this.onClickSelectChat;
    const idArray = chat.id.split('_');
    if (idArray[0] && idArray[0] === this.currentUser.uid) {
      return idArray[1];
    }
    return idArray[0];
  }
  onSendMessage() {
    if (!this.message.value) {
      return;
    }
    this.store.dispatch(new PostAction.SendMessage({ to: this.antherUserId, message: this.message.value, lastMessageId: this.onClickSelectChat.id }))
    this.message.setValue('');
  }

}
