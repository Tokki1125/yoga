import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
// import {_} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';

@Component({
  selector: 'app-partners',
  templateUrl: './partners.component.html',
  styleUrls: ['./partners.component.scss']
})
export class PartnersComponent implements OnInit {

  constructor(private translate: TranslateService) {
    var g_language = window.localStorage.getItem('g_language');
    translate.setDefaultLang(g_language);
  }

  ngOnInit() {
  }
  useLanguage(language: string) {
    this.translate.use(language);
  }

}
