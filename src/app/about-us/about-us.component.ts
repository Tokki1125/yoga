import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';
import { PostState, Aboutus } from 'app/store/post/post.state';
import {TranslateService} from '@ngx-translate/core';
// import {_} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  aboutus: Aboutus;
  constructor(private translate: TranslateService,private store: Store) { 
    var g_language = window.localStorage.getItem('g_language');
    translate.setDefaultLang(g_language);
  }
  useLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit() {
    this.store.dispatch(new PostAction.FetchAboutus());
    this.store.select(PostState.aboutus).subscribe((resp) => {
      if (resp) {
        this.aboutus = resp;
        console.log(resp);
      }

    })
  }

}
