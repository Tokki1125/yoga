import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';
import { PostState, Contact } from 'app/store/post/post.state';
import {TranslateService} from '@ngx-translate/core';
// import {_} from '@biesbjerg/ngx-translate-extract/dist/utils/utils';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  form: FormGroup;
  contact: Contact;
  constructor(private translate: TranslateService,private fb: FormBuilder, private store: Store) {
    var g_language = window.localStorage.getItem('g_language');
    translate.setDefaultLang(g_language);
    this.form = this.fb.group({
      name: [],
      email: [],
      note: [],
      date: [new Date().getTime()]
    })
  }
  useLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit() {
    this.store.dispatch(new PostAction.fetchContact());
    this.store.select(PostState.contact).subscribe((resp) => {
      this.contact = resp;
    })
  }
  onSubmit() {
    this.store.dispatch(new PostAction.DoContact());
    this.form.reset();
  }
}
