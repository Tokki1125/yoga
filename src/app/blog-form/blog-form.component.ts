import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Store } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';

@Component({
  selector: 'app-blog-form',
  templateUrl: './blog-form.component.html',
  styleUrls: ['./blog-form.component.scss']
})
export class BlogFormComponent implements OnInit {

  form: FormGroup;
  constructor(private fb: FormBuilder, private store: Store) {
    this.form = this.fb.group({
      title: [],
      note: [],
      date: [new Date().getTime()],
    })
  }

  ngOnInit() {
  }
  onSubmit() {
    this.store.dispatch(new PostAction.AddBlog());
    this.form.reset();
  }
}
