import { Component, OnInit } from '@angular/core';
import { MatDialogRef, MatDialog } from '@angular/material';
import { NgProgressService } from 'app/ngProgress/ng-progress.service';

@Component({
  selector: 'app-confirm-dailog',
  templateUrl: './confirm-dailog.component.html',
  styleUrls: ['./confirm-dailog.component.scss']
})
export class ConfirmDailogComponent implements OnInit {

  constructor(public progress:NgProgressService) { }

  ngOnInit() {
    console.log('dialoge')
  }
  onNoClick() {
    this.progress.hideConfirmDialog();
    this.progress.clickNo();
  }

  onYesClick(){
    window.open('https://www.allianz-vor-ort.de/landingpage/zsm/25496435-642a-4508-90b6-613840ab9cac', '_blank');
    this.progress.hideConfirmDialog();
    this.progress.clickYes();
  }
}
