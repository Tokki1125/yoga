import { Component, OnInit } from '@angular/core';
import { ContactUs, PostState } from 'app/store/post/post.state';
import { Store } from '@ngxs/store';
import { PostAction } from 'app/store/post/post.actions';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.scss']
})
export class ContactListComponent implements OnInit {

  contactList: ContactUs[] = [];
  constructor(private store: Store) { }

  ngOnInit() {
    this.store.dispatch(new PostAction.FetchContact());
    this.store.select(PostState.contactList).subscribe((resp) => {
      this.contactList = resp;
    })
  }
}
