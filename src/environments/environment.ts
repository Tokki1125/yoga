// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

// export const environment = {
//   production: false,
//   firebase: {
//     apiKey: "AIzaSyCKOYSUUxvX-Ca_BUnQX5N_u_OWhjGYXQ4",
//     authDomain: "rant-app-dfefd.firebaseapp.com",
//     databaseURL: "https://rant-app-dfefd.firebaseio.com",
//     projectId: "rant-app-dfefd",
//     storageBucket: "rant-app-dfefd.appspot.com",
//     messagingSenderId: "770695781431"
//   }
// };


export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBqWUBFIEWr_F9Fz9cplrX9DP9_iw2g5I4",
    authDomain: "rent-app-2be84.firebaseapp.com",
    databaseURL: "https://rent-app-2be84.firebaseio.com",
    projectId: "rent-app-2be84",
    storageBucket: "rent-app-2be84.appspot.com",
    messagingSenderId: "486380614420"
  }
};